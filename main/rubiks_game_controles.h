#ifndef _RUBIKS_GAME_H
#define _RUBIKS_GAME_H

//#include <FastLED.h>
#include "vibration_control.h"
#include "center_control.h"
#include "button_control.h"
#include "game.h"
#include "lib_collidor.h"

#define NUMBER_LEDS 192
#define LANE_END_POS_LENGTH 28
#define RAINBOW_LENGTH 28
#define COLOR_TYPE_AMOUNT 7


struct rubiks_game : game
{
	long	pos_holder_old = -1;
	CENTER_PIECE *center;
	BUTTONS* buttons;
	//CRGBArray<NUMBER_LEDS>* leds;
	//CRGB rainbow_original[RAINBOW_LENGTH]; //becomes initialized at init	
	pixels leds;
	pixels rainbow_original;

	bool rainbow_changed;
	bool skip = 0;
	rubiks_game(CENTER_PIECE *cp, BUTTONS* but, pixels leds)
		: center(cp)
		, buttons(but)
		, leds(leds)
	{
		rainbow_changed = 0;
	}

	virtual void init() override;

	virtual void tick() override;

	virtual void shutdown() override;

	void shuffle();

	void animate_switching(byte pos1, byte pos2, color color1, color color2, byte direction);

	void center_star_animation();

	void switch_pos();

	void rainbow_rotate(bool clockwise, bool counterclockwise);

	void rotate_circle();

	void show_original();

	void show_current();

	void rotate_clockwise();

	void rotate_counterclockwise();

	void star_animation();

	void foo(int start, int end, bool should_inc, color color);

	int light_forward_incr(int i, color color);

	int light_forward_decr(int i, color color);

	color fooz(color color1, color color2);


	int color_choice(unsigned int choice, unsigned int color, int* color_amount);

	void rotate_rainbow_right();

private:
	void decr_lightcross_by_one(int *north, int *east, int *south, int *west);
	void flip_lightcross_right(int north, int east, int south, int west);
};


#endif // !_RUBIKS_GAME_H
