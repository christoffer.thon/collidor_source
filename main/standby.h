#ifndef STANDBY_H
#define STANDBY_H

#include "standby_declariations.h"

int std_yellow = 0xE8F124;
int std_orange = 0xF03316;
int std_green = 0x25E625;
int std_pink = 0xEE24F1;

unsigned long standby_animation_counter;

//there should be only one, should just call cpy_rbw_standby

CRGB rr[] = {
  0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1,   //pink
  0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0,   //Blue
  0x25E625, 0x25E625, 0x25E625, 0x25E625, 0x25E625, 0x25E625, 0x25E625,   //green
  0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124    //yellow
};

int west_and_north_out(int pos)
{
  return pos += 1;
}

int south_and_east_out(int pos)
{
  return pos -= 1;
}

int south_and_east_in(int pos)
{
  return pos += 1;
}

int west_and_north_in(int pos)
{
  return pos -= 1;
}




void west_east_add_light_in(int max_lights, int pause)
{
  int cross_right = EAST;
  int cross_left = WEST;
  int pink = 0;
  int green = 14;
  for (int i = 0; i < max_lights; i++)
  {
    leds[cross_left] = 0;
    leds[cross_right] = 0;
    cross_right = south_and_east_in(cross_right);
    cross_left = west_and_north_in(cross_left);
    leds[cross_left] = rr[pink];
    leds[cross_right] = rr[green];
    delay(pause);
    FastLED.show();
  }
}

void south_north_add_light_out(int max_lights, int pause)
{
  int cross_south = SOUTH + CROSS_DIRECTION_SIZE;
  int cross_north = NORTH - CROSS_DIRECTION_SIZE;

  int yellow = 21;
  int orange = 0xF03316; //orange
  for (int i = 0; i < max_lights; i++)
  {
    leds[cross_south] = 0;
    leds[cross_north] = 0;
    cross_south = south_and_east_out(cross_south);
    cross_north = west_and_north_out(cross_north);
    leds[cross_south] = orange;
    leds[cross_north] = rr[21];
    delay(pause);
    FastLED.show();
  }
}

void east_clean_out()
{
  for (int i = EAST; i <= EAST + CROSS_DIRECTION_SIZE; i++)
  {
    leds[i] = 0;
  }
}

void west_clean_out()
{
  for (int i = WEST; i >= WEST - CROSS_DIRECTION_SIZE; i--)
  {
    leds[i] = 0;
  }
}

void east_west_clean_out()
{
  int east = EAST;
  int west = WEST;
  for (int i = 0; i <= CROSS_DIRECTION_SIZE; i++)
  {
    leds[east] = 0;
    leds[west] = 0;
    east = south_and_east_in(east);
    west = west_and_north_in(west);
    delay(PAUSE_SCREEN_LIGHT_DELAY_MS);
    FastLED.show();
  }
}

void south_north_clean_in()
{
  int north = NORTH - CROSS_DIRECTION_SIZE;
  int south = SOUTH + CROSS_DIRECTION_SIZE;
  for (int i = 0; i <= CROSS_DIRECTION_SIZE; i++)
  {
    leds[north] = 0;
    leds[south] = 0;
    north = west_and_north_out(north);
    south = south_and_east_out(south);
    delay(PAUSE_SCREEN_LIGHT_DELAY_MS);
    FastLED.show();
  }
}

void cross_blink_twice()
{
  int pause = 10;
  east_west_clean_out();
  south_north_clean_in();
  for (int i = CROSS_DIRECTION_SIZE; i > 0; i--)
  {
    south_north_add_light_out(i, pause);
    west_east_add_light_in(i, pause);
  }
  east_west_clean_out();
  south_north_clean_in();
  for (int i = CROSS_DIRECTION_SIZE; i > 0; i--)
  {
    south_north_add_light_out(i, pause);
    west_east_add_light_in(i, pause);
  }
  east_west_clean_out();
  south_north_clean_in();
  //delay(PAUSE_SCREEN_LIGHT_DELAY_MS / 3);
}

void ring_fill(int color, int from, int to)
{
  for (int i = from; i <= to; i++)
  {
    leds[i] = color;
  }
}


void ring_clear(int from, int to)
{
  for (int i = from; i <= to; i++)
  {
    leds[i] = 0;
  }
}

void inner_rings_blink(int blink_amount)
{
  for (int i = 0; i < blink_amount; i++)
  {
    ring_fill(std_yellow, NW_INNER_LOWER, NW_INNER_UPPER);
    ring_fill(std_pink, SW_INNER_LOWER, SW_INNER_UPPER);
    ring_fill(std_orange, SE_INNER_LOWER, SE_INNER_UPPER);
    ring_fill(std_green, NE_INNER_LOWER, NE_INNER_UPPER);
    FastLED.show();
    delay(PAUSE_SCREEN_LIGHT_DELAY_MS * 1.8);
    ring_clear(NW_INNER_LOWER, NW_INNER_UPPER);
    ring_clear(SW_INNER_LOWER, SW_INNER_UPPER);
    ring_clear(SE_INNER_LOWER, SE_INNER_UPPER);
    ring_clear(NE_INNER_LOWER, NE_INNER_UPPER);
    FastLED.show();
  }
}

void standby_animation() {

  for (int i = CROSS_DIRECTION_SIZE; i > 0; i--)
  {
    west_east_add_light_in(i, PAUSE_SCREEN_LIGHT_DELAY_MS);
  }
  east_west_clean_out();
  for (int i = CROSS_DIRECTION_SIZE; i > 0; i--)
  {
    south_north_add_light_out(i, PAUSE_SCREEN_LIGHT_DELAY_MS);
  }
  south_north_clean_in();
  cross_blink_twice();

  inner_rings_blink(5);
}
#endif