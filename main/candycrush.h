#ifndef CANDYCRUSH_HEADER_H
#define CANDYCRUSH_HEADER_H


#include "center_control.h"
#include "lib_collidor.h"
#include "game.h"
#include <Adafruit_NeoPixel.h>
#include "button_control.h"


#define BLINK_SUCCESS 2
#define MIN_SUCCESSES 3
#define AMOUNT_OF_BUTTONS 27
#define COLOR_BLINKS 3
#define MAX_SUCCESSES 19
#define MAX_LANES 2
#define HALF_SECOND 500

//void candycrush(CRGBArray<NUM_LEDS3>* leds);
void light_up_compass();
void random_color_range(int from, int to);

//should be in state_container
//int* sc1_sequantial_positions;
//int* sc2_sequantial_positions;
//sequantial_color* color_lines;


enum class candycrush_state {
	init,
	running,
	shutdown
};

typedef struct positions {
	int pos1;
	int pos2;
	int pos3;
	positions(int pos1, int pos2)
		: pos1(pos1)
		, pos2(pos2)
	{
		//if (pos1 >= 0 && pos2 >= 0)
			//Serial.println("creating positions");
	}

	positions(int pos1, int pos2, int pos3)
		: pos1(pos1)
		, pos2(pos2)
		, pos3(pos3)
	{}
};

typedef struct sequantial_color {
	int* sequantial_positions;
	light_line* line;
	int max_size;
	int size;

	void reset_positions();
	//void release_positions();
	void add_pos(int pos);
	char has_position(int position);
	void sqp_change_light(color color, int delay);
	void move_line_down();
	void move_towards_lower(int decr);
	void move_towards_upper(int decr);
	char is_cross(int lower_check);
	bool color_not_black(color check_color);
};

typedef struct sequantial_colors {
	sequantial_color* color_lines;
	int max_size;
	int size;

	sequantial_colors()
	{
		size = 0;
		max_size = MAX_LANES;
		//should be allocated in state-container && at init
		//Serial.println("allocationg color(s)");
		//color_lines = (sequantial_color*)malloc(sizeof(sequantial_color) * MAX_LANES);
	}

	void add_color_line(sequantial_color color);
	void reset();
};


struct candycrush : game
{
	Adafruit_NeoPixel *pixels;
	//Button prev_selected;
	button_map prev_map;
	BUTTONS* input;

	CENTER_PIECE center;
	candycrush(Adafruit_NeoPixel *leds, BUTTONS* buttons, button_map bmap, CENTER_PIECE center)
		: pixels(leds)
		, input(buttons)
		, prev_map(bmap)
		, center(center)
	{
	}

	virtual void init() override;

	virtual void tick() override;

	virtual void shutdown() override;


	positions register_buttons();
	void check_success(positions buttons_pos);
	sequantial_color success_vertical(int pos, sequantial_color sc);
	void successes_move_row_vertical();

	void should_reset_game();
};

#endif // !CANDYCRUSH_HEADER_H


