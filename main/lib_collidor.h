#ifndef LIB_COLLIDOR_H
#define LIB_COLLIDOR_H

#define NUM_LEDS3 192
#define NUM_REGIONS 4
#define NUM_LINES 4

#include <Adafruit_NeoPixel.h>
#include "current_hardware_version.h"
#include <stdlib.h>
#include <assert.h>
#include "button_control.h"
#include "center_control.h"
#include "shared_constants.h"


struct light_line {
	int pos_lower;
	int pos_upper;

	light_line(int lower, int upper)
		: pos_lower(lower)
		, pos_upper(upper)
	{}
	light_line() {}
	char within_line(int pos);
};

struct light_region {
	light_line inner;
	light_line middle;
	light_line outer;
	light_line cross;
	light_line* lines;
	light_region(light_line cross, light_line inner, light_line middle, light_line outer)
		: cross(cross)
		, inner(inner)
		, middle(middle)
		, outer(outer)
	{
		lines = new light_line[4];
		lines[0] = cross;
		lines[1] = inner;
		lines[2] = middle;
		lines[3] = outer;
	}
	light_region() {}

private:
	//don't use this required default constructor
	light_region(light_line cross, light_line inner, light_line middle, light_line outer, light_line* lines)
		: cross(cross)
		, inner(inner)
		, middle(middle)
		, outer(outer)
		, lines(lines)
	{}
};


struct compass {
	light_region se;
	light_region ne;
	light_region nw;
	light_region sw;
	light_region* regions;	//set me later
	static compass* light_compass;

	compass(light_region se, light_region ne, light_region nw, light_region sw)
		: se(se)
		, ne(ne)
		, nw(nw)
		, sw(sw)
	{
		regions = new light_region[4];
		regions[0] = se;
		regions[1] = ne;
		regions[2] = nw;
		regions[3] = sw;
	}

	static compass* get_compass();
	light_line* get_line(int pos);
	static void instantiate_compass();
	void turn_off(int start, int end, int milis_delay);

private:
	compass(const compass& source) {}
	compass(compass&& source) {}
};

struct job {
	int strength;
	uint32_t end_time;
};

struct vibrator {
	job jobs[VIBRATOR_JOBLIST_SIZE];

	vibrator()
	{
		memset(jobs, 0, sizeof(job)*VIBRATOR_JOBLIST_SIZE);		
	}

	void check_jobs();
	int highest_strength();
	void add_job(int strenght, uint32_t time);
	void vibrate(int strength, int milis);
	void update();

};

Adafruit_NeoPixel* set_leds(Adafruit_NeoPixel* leds);
void set_random_color(int pos);
void fade_up(int start, int end, int pause);
void fade_down(int start, int end, int pause);
void blink_light(int pos, int color, int time);
void swap_lights(int pos_first, int color_first, int pos_second, int color_second);
color num_to_color(int num);

Adafruit_NeoPixel* get_leds();
BUTTONS* set_buttons(BUTTONS* buttons);
BUTTONS* get_buttons();
color get_color(int pos);
void blink_light_off_first(int pos, int color, int time);
void fade_down_led(int pos, int time);
void blinker(int pos, int blinks, int time);
void light_move_crgb(int pos, color c);
void blink(int* positions, int pos_size, int miliseconds, int qty);


void light_move_forward(int pos, int end, color color);
void light_move_backwards(int start, int end, color color);

CENTER_PIECE* set_center(CENTER_PIECE* center);
CENTER_PIECE* get_center();


#endif LIB_COLLIDOR_H