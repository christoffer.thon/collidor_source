#include "rubiks_game_controles.h"
long old_pos_holder;
color temp_rainbow_holder[RAINBOW_LENGTH] = { 0 };

byte centerSteps = 23;
bool game2_init_flag = 1;
unsigned int init_counter = 0;

//LANE_END_POS_LENGTH positions
constexpr int rsouth = 21, reast = 24, rwest = 14, rnorth = 7; 
int LANE_END_POS[LANE_END_POS_LENGTH] = { 24, 67, 136, 174, 176, 147, 85, 23, 86, 148, 177, 179, 159, 104, 47, 105, 160, 180, 182, 171, 123, 0, 48, 124, 172, 173, 135, 66 };

//pink, //Blue, //green  //yellow
color rainbow[RAINBOW_LENGTH] = { 0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1, 0xEE24F1, 0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0, 0x0224E0, 0x25E625, 0x25E625, 0x25E625, 0x25E625, 0x25E625, 0x25E625, 0x25E625,  0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124, 0xE8F124 };
//Remove in prod
byte current_hall1, current_hall2;

//implement center

/*
	)tick: 
		should add another conditional to check whether circle is prober turned
	)star animation: 
		should be able to click on buttons, while circle is hovering 
		can choose animation w/o moving all lights
	)tick: 
		find a way to make logic for counter/clock wise circle 
*/

void rubiks_game::init()
{
	set_leds(leds);
	memcpy(rainbow_original, rainbow, sizeof(rainbow[0]) * RAINBOW_LENGTH);

	for (int i = 0; i <= 27; i++)
	{
		//animate rainbow in a orderly pattern
		light_move_crgb(LANE_END_POS[i], rainbow[i]);
		get_leds()->show();
		//FastLED.show();
		delay(40);
	}
	shuffle();

	//current_hall1 = center->newReading_hall1;
	//current_hall2 = center->newReading_hall2;

	//Serial.println("finished initializing");
}

void rubiks_game::tick()
{
	
	// 3) if(hall1 && hall2 && cross_pressed()) :: star_animatio() 
	// 1) else if(hall1 && !hall2) :: rotate_counterclockwise()
	// 2) else if(!hall2 && hall1) :: rotate_clockwise()

	//if (buttons->is_down(Button::EastZero))
	//{
	//	star_animation();
	//	skip = 1;
	//}
	//if (current_hall1 != center->newReading_hall1 && current_hall2 != center->newReading_hall2 && center->newReading_hall5 == 0)
	//{
	//	Serial.println("rotation clockwise");
	//	rotate_clockwise();
	//	current_hall1 = center->newReading_hall1;
	//	current_hall2 = center->newReading_hall2;
	//}
	//else if(0)
	//{
	//	//rotate_counterclockwise();

	//}
	//returns 0 when center is on
	bool center_off = center->center_exists();
	if (center_off && !rainbow_changed)
	{
		rainbow_changed = 1;
		show_original();
	}
	else if (!center_off && rainbow_changed)
	{
		show_current();
		rainbow_changed = 0;
	}

	if (buttons->first_press() != nullptr)
	{
		switch_pos();
	}
}

void rubiks_game::rotate_counterclockwise()
{
	color tmp = rainbow[0];
	color tmp2;
	for (int i = 1; i < RAINBOW_LENGTH; i++)
	{
		tmp2 = rainbow[i];
		rainbow[i] = tmp;
		tmp = tmp2;
		light_move_crgb(LANE_END_POS[i], rainbow[i]);
	}
	rainbow[0] = tmp;
	get_leds()->show();
}

void rubiks_game::rotate_clockwise()
{
	auto tmp = rainbow[RAINBOW_LENGTH - 1];
	color tmp2 = 0;
	for (int i = RAINBOW_LENGTH - 1; i > 0; i--)
	{
		tmp2 = rainbow[i];
		rainbow[i] = tmp;
		tmp = tmp2;
		light_move_crgb(LANE_END_POS[i], rainbow[i]);
	}
	rainbow[RAINBOW_LENGTH - 1] = tmp;
	//Serial.println("end of circle turn");
	get_leds()->show();
}

void rubiks_game::star_animation()
{
	int s_start = SOUTH, e_start = EAST, n_start = NORTH, w_start = WEST;
	int light_speed = 15;

	for (int i = 0; i <= CROSS_DIRECTION_SIZE; i++)
	{
		s_start = light_forward_incr(s_start, rainbow[rsouth]);
		e_start = light_forward_incr(e_start, rainbow[reast]);
		w_start = light_forward_decr(w_start, rainbow[rwest]);
		n_start = light_forward_decr(n_start, rainbow[rnorth]);
		delay(light_speed);
	}

	//use new reader
	//while (center->newReading_hall3 == 0)
	//{
	//	center->update(0);
	//}
	//After setting the last light, it needs to go down by one .. don't ask why
	decr_lightcross_by_one(&n_start, &e_start, &s_start, &w_start);
	flip_lightcross_right(n_start, e_start, s_start, w_start);
	rotate_rainbow_right();
	
	get_leds()->show();
	delay(450);
	
	//this is an abonimation and should be deleted once we're done recording xddddddddddddddddd
	//center->newReading_hall3 = 0;
	
	for (int i = 0; i < CROSS_DIRECTION_SIZE + 1; i++)
	{
		s_start = light_forward_decr(s_start, rainbow[rsouth]);
		e_start = light_forward_decr(e_start, rainbow[reast]);
		w_start = light_forward_incr(w_start, rainbow[rwest]);
		n_start = light_forward_incr(n_start, rainbow[rnorth]);
		delay(light_speed);
	}
}

void rubiks_game::rotate_rainbow_right()
{
	auto tmp = rainbow[rsouth];
	rainbow[rsouth] = rainbow[reast];
	auto tmp2 = rainbow[rwest];
	rainbow[rwest] = tmp;
	tmp = rainbow[rnorth];
	rainbow[rnorth] = tmp2;
	tmp2 = rainbow[reast];
	rainbow[reast] = tmp;
}

void rubiks_game::flip_lightcross_right(int n_start, int e_start, int s_start, int w_start)
{
	light_move_crgb(s_start, rainbow[reast]);
	light_move_crgb(e_start, rainbow[rnorth]);
	light_move_crgb(n_start, rainbow[rwest]);
	light_move_crgb(w_start, rainbow[rsouth]);
}

void rubiks_game::decr_lightcross_by_one(int *n_start, int *e_start, int *s_start, int *w_start)
{
	*n_start = (*n_start) + 1;
	*w_start = (*w_start) + 1;
	*e_start = (*e_start) - 1;
	*s_start = (*s_start) - 1;
}

color rubiks_game::fooz(color color1, color color2)
{
	auto tmp = rainbow[color2];
	rainbow[color2] = rainbow[color1];
	return tmp;
}

int rubiks_game::light_forward_incr(int i, color color)
{
	light_move_crgb(i, color);
	light_move_crgb(i - 1, 0);
	get_leds()->show();
	return i + 1;
}

int rubiks_game::light_forward_decr(int i, color color)
{
	light_move_crgb(i, color);
	light_move_crgb(i + 1, 0);
	get_leds()->show();
	return i - 1;
}


void rubiks_game::show_current()
{
	for (int i = 0; i < RAINBOW_LENGTH - 1; i++)
	{
		light_move_crgb(LANE_END_POS[i], rainbow[i]);
	}
	get_leds()->show();
}

void rubiks_game::show_original()
{
	for (int i = 0; i <= 27; i++)
	{
		light_move_crgb(LANE_END_POS[i], rainbow_original->getPixelColor(i));
		//animate rainbow in a orderly patton
	}
	get_leds()->show();
}

void rubiks_game::rotate_circle()
{
	//int pos_holder = center->rotation_pos();
	/*if (pos_holder > old_pos_holder)
	{

		rainbow_rotate(1, 0);
		old_pos_holder = pos_holder;

		for (int i = 0; i <= 27; i++) {
			light_move_crgb(LANE_END_POS[i], rainbow[i]);
		}
		FastLED.show();
	}
	else if (pos_holder < old_pos_holder)
	{

		rainbow_rotate(0, 1);
		old_pos_holder = pos_holder;

		for (int i = 0; i <= 27; i++) {
			light_move_crgb(LANE_END_POS[i], rainbow[i]);
		}
		FastLED.show();
	}*/
}

void rubiks_game::shutdown()
{
	//release both arrays
}

//0xEE24F1 //pink
//0x0224E0 //blue
//0x25E625 //green
//0xE8F124 //yellow
void rubiks_game::shuffle()
{
	int total_yellow = 0, total_green = 0, total_pink = 0, total_blue = 0;
	unsigned int colors[4]{ 0xEE24F1, 0x0224E0 , 0x25E625 , 0xE8F124 };
	int i = 0;
	//ensure 7 of each color
	while (i < RAINBOW_LENGTH)
	{
		//refactor this into a function using *colors and *temp_rainbow_holder
		unsigned int choice = colors[random(4)];
		if (color_choice(choice, 0xEE24F1, &total_pink))
		{
			temp_rainbow_holder[i] = choice;
			i++;
		}
		else if (color_choice(choice, 0x0224E0, &total_blue))
		{
			temp_rainbow_holder[i] = choice;
			i++;
		}
		else if (color_choice(choice, 0x25E625, &total_green))
		{
			temp_rainbow_holder[i] = choice;
			i++;
		}
		else if (color_choice(choice, 0xE8F124, &total_yellow))
		{
			temp_rainbow_holder[i] = choice;
			i++;
		}
	}

	for (int y = 27; y >= 0; y--)
	{
		rainbow[y] = temp_rainbow_holder[y];
		light_move_crgb(LANE_END_POS[y], rainbow[y]);

		delay(30);
		get_leds()->show();
		//FastLED.show();

	}
}

int rubiks_game::color_choice(unsigned int choice, unsigned int color, int* color_amount)
{
	if ((choice & color) == color && (*color_amount) < 7)
	{
		(*color_amount)++;
		return 1;
	}
	return 0;
}

void rubiks_game::animate_switching(byte pos1, byte pos2, color color1, color color2, byte direction)
{
	//vibrate(1);

	//Serial.println(direction);
	byte steps = pos2 - pos1;

	if (direction == 1) {
		//this is blink... 
		for (int x = 0; x < steps; x++) {

			light_move_crgb(pos1 + x, color1);
			light_move_crgb(pos2 - x, color2);
			/*leds[pos1 + x] = color1;
			leds[pos2 - x] = color2;*/

			get_leds()->show();
			delay(22);

			light_move_crgb(pos1 + x, 0);
			light_move_crgb(pos2 - x, 0);

			//leds[pos1 + x] = CRGB::Black;
			//leds[pos2 - x] = CRGB::Black;
		}
		light_move_crgb(pos1, color2);
		light_move_crgb(pos2, color1);

		//leds[pos1] = color2;
		//leds[pos2] = color1;
	}
	else if (direction == 0) {
		//refactor and use blink ... 
		for (int x = 0; x < steps; x++) {

			//leds[pos1 + x] = color2;
			//leds[pos2 - x] = color1;
			light_move_crgb(pos1 + x, color1);
			light_move_crgb(pos2 - x, color2);

			get_leds()->show();
			delay(22);

			/*	leds[pos1 + x] = CRGB::Black;
				leds[pos2 - x] = CRGB::Black;*/
			light_move_crgb(pos1 + x, 0);
			light_move_crgb(pos2 - x, 0);
		}
		light_move_crgb(pos1, color2);
		light_move_crgb(pos2, color1);
		/*leds[pos1] = color2;
		leds[pos2] = color1;*/

	}
	else if (direction == 3) {

		for (int x = 0; x < (steps / 2); x++) {

			light_move_crgb(pos1 + x, color2);
			light_move_crgb(pos2 - x, color1);

			/*leds[pos1 + x] = color1;
			leds[pos2 - x] = color2;*/

			get_leds()->show();
			delay(22);

			light_move_crgb(pos1 + x, 0);
			light_move_crgb(pos2 - x, 0);

			//leds[(pos1 + x)] = CRGB::Black;
			//leds[(pos2 - x)] = CRGB::Black;

		}
		/*
		  for (int x = 0; x < (steps / 2); x++) {

		  leds[(pos1 + steps / 2) - x] =  color1;
		  leds[(pos2 - steps / 2) + x] =  color2;

		  FastLED.show();
		  delay(22);

		  leds[(pos1 - steps / 2) - x] =  CRGB::Black;
		  leds[(pos2 + steps / 2) + x] =  CRGB::Black;

		  }
		  /*/

		  //leds[(pos1 / 2) - 1] =  color1;
		  //leds[(pos2 / 2) + 1] =  color2;
	}

	get_leds()->show();
}

void rubiks_game::center_star_animation()
{

	for (int x = 0; x <= (centerSteps / 2); x++) {

		leds[0 + x] = rainbow[21];
		leds[23 - x] = rainbow[7];
		leds[24 + x] = rainbow[0];
		leds[47 - x] = rainbow[14];

		get_leds()->show();
		delay(22);

		leds[0 + x] = 0;
		leds[23 - x] = 0;
		leds[24 + x] = 0;
		leds[47 - x] = 0;
	}

	leds[11] = rainbow[21];
	leds[12] = rainbow[7];
	leds[35] = rainbow[0];
	leds[36] = rainbow[14];

	get_leds()->show();

	int oldRotationPos = center->rotation_pos();
	while (center->rotation_pos() == oldRotationPos) {}

	leds[11] = 0;
	leds[12] = 0;
	leds[35] = 0;
	leds[36] = 0;

	get_leds()->show();
	//FastLED.show();

	if (center->rotation_pos() > oldRotationPos) {

		for (int x = (centerSteps / 2); x >= 0; x--) {

			leds[0 + x] = rainbow[0];
			leds[23 - x] = rainbow[14];
			leds[24 + x] = rainbow[7];
			leds[47 - x] = rainbow[21];

			get_leds()->show();
			//FastLED.show();
			delay(22);

			leds[0 + x] = 0;
			leds[23 - x] = 0;
			leds[24 + x] = 0;
			leds[47 - x] = 0;
		}

		leds[0] = rainbow[0];
		leds[23] = rainbow[14];
		leds[24] = rainbow[7];
		leds[47] = rainbow[21];

		rainbow[0] = rainbow[7];
		rainbow[7] = rainbow[14];
		rainbow[14] = rainbow[21];
		rainbow[21] = rainbow[0];

		get_leds()->show();
	}
	else if (center->rotation_pos() < oldRotationPos) {

	}
}

void rubiks_game::switch_pos()
{
//	//rf
//	String button_states = buttons->display_pin_values(1); //should be refacted to use buttons.button_update instead
//
//	if (button_states.substring(12, 13) == "1" || button_states.substring(2, 3) == "1") {
//
//		auto temp_color1 = rainbow[8];
//		auto temp_color2 = rainbow[13];
//
//		rainbow[8] = temp_color2;
//		rainbow[13] = temp_color1;
//
//		animate_switching(86, 104, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(3, 4) == "1" || button_states.substring(19, 20) == "1") {
//
//		auto temp_color1 = rainbow[0];
//		auto temp_color2 = rainbow[14];
//
//		//animate_switching(24, 47, temp_color1, temp_color2, 1);
//
//		//Serial.println(center.center_alignment());
//
//		// when alignmet "left" go to holding pos //
//		if (center->center_alignment() == "left") {
//			center_star_animation();
//		}
//		else if (center->center_alignment() == "right") {
//			animate_switching(24, 47, temp_color1, temp_color2, 1);
//		}
//
//		rainbow[0] = temp_color2;
//		rainbow[14] = temp_color1;
//
//	}
//
//	else if (button_states.substring(0, 1) == "1" || button_states.substring(14, 15) == "1") {
//
//		auto temp_color1 = rainbow[10];
//		auto temp_color2 = rainbow[11];
//
//		rainbow[10] = temp_color2;
//		rainbow[11] = temp_color1;
//
//		animate_switching(177, 179, temp_color1, temp_color2, 1);
//		delay(300); //small lane
//	}
//
//	else if (button_states.substring(27, 28) == "1" || button_states.substring(11, 12) == "1") {
//
//		auto temp_color1 = rainbow[7];
//		auto temp_color2 = rainbow[21];
//
//		rainbow[7] = temp_color2;
//		rainbow[21] = temp_color1;
//
//		animate_switching(0, 23, temp_color1, temp_color2, 1);
//
//		/*
//		  if (center.center_alignment() == "left") {
//		  animate_switching(0, 23, temp_color1, temp_color2, 0);
//		  }
//		  else if (center.center_alignment() == "right") {
//		  animate_switching(0, 23, temp_color1, temp_color2, 3);
//		  }
//		*/
//	}
//
//	else if (button_states.substring(1, 2) == "1" || button_states.substring(13, 14) == "1") {
//
//		auto temp_color1 = rainbow[9];
//		auto temp_color2 = rainbow[12];
//
//		rainbow[9] = temp_color2;
//		rainbow[12] = temp_color1;
//
//		animate_switching(148, 159, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(10, 11) == "1" || button_states.substring(20, 21) == "1") {
//
//		auto temp_color1 = rainbow[1];
//		auto temp_color2 = rainbow[6];
//
//		rainbow[1] = temp_color2;
//		rainbow[6] = temp_color1;
//
//		animate_switching(67, 85, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(9, 10) == "1" || button_states.substring(21, 22) == "1") {
//
//		auto temp_color1 = rainbow[2];
//		auto temp_color2 = rainbow[5];
//
//		rainbow[2] = temp_color2;
//		rainbow[5] = temp_color1;
//
//		animate_switching(136, 147, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(8, 9) == "1" || button_states.substring(22, 23) == "1") {
//
//		auto temp_color1 = rainbow[3];
//		auto temp_color2 = rainbow[4];
//
//		rainbow[3] = temp_color2;
//		rainbow[4] = temp_color1;
//
//		animate_switching(174, 176, temp_color1, temp_color2, 1);
//		delay(300); //small lane
//	}
//
//	else if (button_states.substring(18, 19) == "1" || button_states.substring(28, 29) == "1") {
//
//		auto temp_color1 = rainbow[22];
//		auto temp_color2 = rainbow[27];
//
//		rainbow[22] = temp_color2;
//		rainbow[27] = temp_color1;
//
//		animate_switching(48, 66, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(17, 18) == "1" || button_states.substring(29, 30) == "1") {
//
//		auto temp_color1 = rainbow[23];
//		auto temp_color2 = rainbow[26];
//
//		rainbow[23] = temp_color2;
//		rainbow[26] = temp_color1;
//
//		animate_switching(124, 135, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(4, 5) == "1" || button_states.substring(26, 27) == "1") {
//
//		auto temp_color1 = rainbow[15];
//		auto temp_color2 = rainbow[20];
//
//		rainbow[15] = temp_color2;
//		rainbow[20] = temp_color1;
//
//		animate_switching(105, 123, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(5, 6) == "1" || button_states.substring(25, 26) == "1") {
//
//		auto temp_color1 = rainbow[16];
//		auto temp_color2 = rainbow[19];
//
//		rainbow[16] = temp_color2;
//		rainbow[19] = temp_color1;
//
//		animate_switching(160, 171, temp_color1, temp_color2, 1);
//	}
//
//	else if (button_states.substring(6, 7) == "1" || button_states.substring(24, 25) == "1") {
//
//		auto temp_color1 = rainbow[16];
//		auto temp_color2 = rainbow[19];
//
//		rainbow[17] = temp_color2;
//		rainbow[18] = temp_color1;
//
//		animate_switching(180, 182, temp_color1, temp_color2, 1);
//		delay(300); //small lane
//	}
//}
//
//void rubiks_game::rainbow_rotate(bool clockwise, bool counterclockwise) {
//
//	if (clockwise) {
//
//		vibrate(4);
//
//		for (int z = 0; z <= 27; z++) {
//
//			if (z == 0) {
//				rainbow[28] = rainbow[0];
//			}
//			rainbow[z] = rainbow[z + 1];
//			get_leds()->show();
//		}
//	}
//	if (counterclockwise) {
//
//		vibrate(4);
//
//		for (int z = 27; z >= 0; z--) {
//
//			if (z == 27) {
//				rainbow[-1] = rainbow[27];
//			}
//			rainbow[z] = rainbow[z - 1];
//			get_leds()->show();
//		}
//		// Serial.println();
//	}
}

//void rubiks_game::game_2_init_sequence() {
//
//	if (game2_init_flag) {
//
//		if (init_counter == 0) {
//
//			for (int i = 0; i <= 27; i++) {
//
//				//animate rainbow in a orderly patton
//				leds[LANE_END_POS[i]] = rainbow[i];
//				FastLED.show();
//				delay(40);
//			}
//		}
//		else if (init_counter == 250) {
//
//			shuffle();
//
//			game2_init_flag = 0;
//			init_counter = 0;
//
//		}
//
//		init_counter++;
//
//	}
//}

