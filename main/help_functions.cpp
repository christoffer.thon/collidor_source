#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

#include "Arduino.h"
//#include "FastLED.h"
#include <Adafruit_NeoPixel.h>
#include "help_functions.h"
#include "shared_constants.h"

void foo(pixels leds, int start, int finish)
{
	for (int i = start; i <= finish; i++)
	{
		leds->setPixelColor(i, std_yellow);
		leds->setBrightness(1);
		leds->show();
		Serial.print("i: ");
		Serial.print(i);
		Serial.println();

		delay(250);
	}
}

void copy(int* src, int* dst) {
	memcpy(dst, src, sizeof(src[0]) * 2);
}

//void copy_colors(color src, color dst) {
//	memcpy(dst, src, sizeof(src[0]) * 2);
//}

#endif // !HELP_FUNCTIONS_H
