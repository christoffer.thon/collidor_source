#ifndef MENU_H
#define MENU_H


#include "shared_constants.h"
#include "center_control.h"
#include "button_control.h"
#include "game.h"
#include <Adafruit_NeoPixel.h>

#include "candycrush.h"
//#include "rubiks_game_controles.h"



struct Menu
{
	int rotary_constrain_value;
	CENTER_PIECE* center;
	
	Adafruit_NeoPixel* pixels;

	BUTTONS* buttons;

	Menu(Adafruit_NeoPixel *pixels, BUTTONS *buttons, CENTER_PIECE* center);

	void init();
	byte choose_player_amount();
	game* choose_mode(button_map* choice);
	//byte choose_game_mode();
};

#endif 