#ifndef SHARED_CONSTANTS_H
#define SHARED_CONSTANTS_H

#define NUM_LEDS 176

#include <Adafruit_NeoPixel.h>

//define board_vemos using extern 
int constexpr std_yellow = 0xE8F124;
int constexpr std_orange = 0xF03316;
int constexpr std_green = 0x25E625;
int constexpr std_pink = 0xEE24F1;

// flow speed
int constexpr flow_fast = 25;
int constexpr flow_medium = 50;
int constexpr flow_slow = 100;

// blik speeds
int constexpr blink_fast = 25;
int constexpr blink_medium = 50;
int constexpr blink_slow = 100;


int constexpr np_brightness = 1;

typedef Adafruit_NeoPixel* pixels; //remove???
typedef uint32_t color;

#endif