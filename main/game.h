#ifndef _GAME_H
#define _GAME_H

typedef struct
{
	virtual void init()
	{
		Serial.println("init not implemented");
	}
	virtual void tick()
	{
		Serial.println("tick not implemented");
	}
	virtual void shutdown() //go back to start
	{
		Serial.println("shutdown not implemented");
	}
}game;

#endif // !_GAME_H
