#include "lib_collidor.h"

BUTTONS* buttonsss;
Adafruit_NeoPixel* ledsss;
compass* compass::light_compass;
CENTER_PIECE* center_pieceee;

CENTER_PIECE* get_center()
{
	return center_pieceee;
}

CENTER_PIECE* set_center(CENTER_PIECE* center)
{
	center_pieceee = center;
	return center_pieceee;
}

BUTTONS* set_buttons(BUTTONS *buttons)
{
	buttonsss = buttons;
	return buttonsss;
}

BUTTONS* get_buttons()
{
	return buttonsss;
}

Adafruit_NeoPixel* set_leds(Adafruit_NeoPixel* leds)
{
	ledsss = leds;
	return ledsss;
}

Adafruit_NeoPixel* get_leds()
{
	return ledsss;
}

void light_move_forward(int pos, int end, color color)
{
	for (int i = pos; i <= end; i++)
	{
		light_move_crgb(i, color);
	}
}

void light_move_backwards(int start, int end, color color)
{
	for (int i = start; i > end; i--)
	{
		light_move_crgb(i, color);
	}
}

void display_light(int pause)
{
	get_leds()->show();
	delay(pause);
}

void fade_up(int start, int end, int pause)
{
	for (int i = start; i < end; i++)
	{
		ledsss->setBrightness(i);
		ledsss->show();
		delay(pause);
	}
}

void blinker(int pos, int blinks, int time)
{
	//CRGB tmp = (*ledsss)[pos];
	auto tmp = ledsss->getPixelColor(pos);
	for (int i = 0; i < blinks; i++)
	{
		fade_down_led(pos, time);
		//(*ledsss)[pos] = tmp;
		ledsss->setPixelColor(pos, tmp);
		delay(time);
		ledsss->show();
		//FastLED.show();
	}
}

//255 = black, 0 = white
//A constant fading is made because values seems to be unrealiable
void fade_down_led(int pos, int time)
{
	int fade = 25;
	int incr_by = 10;
	int steps = 10;
	for (int i = 0; i < steps; i++)
	{
		ledsss->setPixelColor(i, fade);
		fade += incr_by;
		delay(time);
		ledsss->show();
	}
}


//miliseconds2: woulde be able to defer between turned off and turn on
void blink(int* positions, int pos_size, int miliseconds, int qty)
{
	//CRGB* colors = new CRGB[pos_size];
	//
	//int i = 0;
	//for (int i = 0; i < pos_size; i++)
	//{
	//	colors[i] = (*ledsss)[positions[i]];
	//}

	//for (int j = 0; j < qty; j++)
	//{
	//	for (int k = 0; k < pos_size; k++)
	//	{
	//		light_move_crgb(positions[k], 0);
	//	}
	//	delay(miliseconds);
	//	FastLED.show();

	//	for (int p = 0; p < pos_size; p++)
	//	{
	//		light_move_crgb(positions[p], colors[p]);
	//		//Serial.print("r:"); Serial.print(colors[p].r); Serial.print(" g:"); Serial.print(colors[p].g); Serial.print("b:"); Serial.println(colors[p].b);

	//	}

	//	delay(miliseconds);
	//	FastLED.show();
	//}
	//Serial.println("freeing colors");
	//free(colors);
}

void fade_down(int start, int end, int pause)
{
	for (int i = end; i > start; i--)
	{
		Serial.println(i);
		ledsss->setBrightness(i);
		ledsss->show();
		delay(pause);
	}
	//Serial.println(FastLED.getBrightness());
	//Serial.println("----");

}

void swap_lights(int pos_first, int color_first, int pos_second, int color_second)
{
	light_move_crgb(pos_second, color_first);
	light_move_crgb(pos_first, color_second);
}

void blink_light(int pos, int color, int time)
{
	light_move_crgb(pos, color);
	delay(time);
	ledsss->show();
	//FastLED.show();
}

void blink_light_off_first(int pos, int color, int time)
{
	light_move_crgb(pos, 0);
	ledsss->show();
	delay(time);
	light_move_crgb(pos, color);
	ledsss->show();
}

void set_random_color(int pos)
{
	int max_number = 3;
	int min_number = 0;
	int choice = rand() % (max_number + 1 - min_number) + min_number;
	/*Serial.println(choice);
	Serial.println("-----------");*/
	light_move_crgb(pos, num_to_color(choice));

}

//if we want to expand colors, this should be removed and a proper random function for each enum need to be implemented 
color num_to_color(int num)
{
	switch (num)
	{
	case 0:
		return std_yellow;
	case 1:
		return std_orange;
	case 2:
		return std_green;
	case 3:
		return std_pink;
	}
}

void light_move_crgb(int pos, color color)
{
	ledsss->setPixelColor(pos, color);
}


void light_move_circular(int pos, int end, int color)
{
	//unimplemented
}

uint32_t get_color(int pos)
{
	return ledsss->getPixelColor(pos);
} 

void compass::instantiate_compass()
{
	light_line se_cross(SOUTH_LOWER, SOUTH_UPPER);
	light_line se_inner(SE_INNER_LOWER, SE_INNER_UPPER);
	light_line se_middle(SE_MIDDLE_LOWER, SE_MIDDLE_UPPER);
	light_line se_outer(SE_OUTER_LOWER, SE_OUTER_UPPER);
	light_region se_region(se_cross, se_inner, se_middle, se_outer);

	//light_cross ne_cross(1, EAST);
	light_line ne_cross(EAST_LOWER, EAST_UPPER);
	light_line ne_inner(NE_INNER_LOWER, NE_INNER_UPPER);
	light_line ne_middle(NE_MIDDLE_LOWER, NE_MIDDLE_UPPER);
	light_line ne_outer(NE_OUTER_LOWER, NE_OUTER_UPPER);
	light_region ne_region(ne_cross, ne_inner, ne_middle, ne_outer);

	light_line nw_cross(NORTH_LOWER, NORTH_UPPER);
	light_line nw_inner(NW_INNER_LOWER, NW_INNER_UPPER);
	light_line nw_middle(NW_MIDDLE_LOWER, NW_MIDDLE_UPPER);
	light_line nw_outer(NW_OUTER_LOWER, NW_OUTER_UPPER);
	light_region nw_region(nw_cross, nw_inner, nw_middle, nw_outer);

	light_line sw_cross(WEST_LOWER, WEST_UPPER);
	light_line sw_inner(SW_INNER_LOWER, SW_INNER_UPPER);
	light_line sw_middle(SW_MIDDLE_LOWER, SW_MIDDLE_UPPER);
	light_line sw_outer(SW_OUTER_LOWER, SW_OUTER_UPPER);
	light_region sw_region(sw_cross, sw_inner, sw_middle, sw_outer);

	compass::light_compass = new compass(se_region, ne_region, nw_region, sw_region);
	
}



light_line* compass::get_line(int pos)
{
	//Serial.print("pos from getline:"); Serial.println(pos);
	for (int i = 0; i < NUM_REGIONS; i++)
	{
		light_region region = compass::get_compass()->regions[i];

		for (int j = 0; j < NUM_LINES; j++)
		{
			if (region.lines[j].within_line(pos))
				return &region.lines[j];
		}
	}
	return nullptr;
}




char light_line::within_line(int pos_id)
{
	for (int i = light_line::pos_lower; i <= light_line::pos_upper; i++)
	{
		if (pos_id == i)
			return 1;
	}
	return 0;
}

compass* compass::get_compass()
{
	if (compass::light_compass == nullptr)
	{
		compass::instantiate_compass();
	}
	return compass::light_compass;
}

void vibrator::check_jobs()
{
	for (int i = 0; i < VIBRATOR_JOBLIST_SIZE; i++)
	{
		if (jobs[i].end_time < millis())
		{
			jobs[i].end_time = 0;
			jobs[i].strength = 0;
		}
	}
}

int vibrator::highest_strength()
{
	int strength = 0;
	for (int i = 0; i < VIBRATOR_JOBLIST_SIZE; i++)
	{
		if (strength < jobs[i].strength)
		{
			strength = jobs[i].strength;
		}
	}
	return strength;
}

void compass::turn_off(int start, int end, int milis_delay)
{
	for (int i = start; i < end; i++)
	{
		light_move_crgb(i, 0);
	}
	get_leds()->show();
	delay(milis_delay);
}


void vibrator::update()
{
	check_jobs();

	//vibrate
	digitalWrite(VIBRATOR_PIN, highest_strength());
}

void vibrator::add_job(int strength, uint32_t time)
{
	job new_job{ strength, millis() + time};
	for (int i = 0; i < VIBRATOR_JOBLIST_SIZE; i++)
	{
		if (jobs[i].end_time == 0)
		{			
			jobs[i] = new_job;
			return;
		}
	}
	
#ifdef _DEBUG
	Serial.print("JobList is to short");
	delay(HALF_SECOND / 2);
	//assert(false && "jobList is to short");
	assert(false);
#endif
}


