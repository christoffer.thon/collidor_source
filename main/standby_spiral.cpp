#include "standby_spiral.h"

jmp_buf buf;
int calls;

int received_input()
{
	/*String filter = "1";
	String button_states = buttons_2.display_pin_values(1);
	int pressed = button_states.indexOf(filter);
	Serial.println(pressed);*/
	//return pressed >= 0 ? 1 : 0;
	return -1;
}

void light_turn(int color_scheme, int pos)
{	
	if (received_input())
	{
		Serial.println("called jmp");
		longjmp(buf, 1);
	}
	get_leds()->setPixelColor(pos, color_scheme);
}


void light_next(int color_scheme, int pos, int prev_pos)
{
	light_turn(0, prev_pos);
	light_turn(color_scheme, pos);
	delay(minimal_delay);
	get_leds()->show();
}

void circle_layer(int color_scheme, int pos_lower, int pos_upper, int offset)
{
	light_turn(color_scheme, pos_lower + offset);
	light_turn(color_scheme, pos_upper - offset);
}


light_line_pos light_line_pos_inst(int lower, int upper)
{
	light_line_pos ld = {.pos_lower = lower, .pos_upper = upper };
	return ld;
}

light_layer light_layer_inst(cross_direction compass, light_line_pos inner, light_line_pos middle, light_line_pos outer, int color)
{
	light_layer ll = { .cross = compass, .inner = inner, .middle = middle, .outer = outer, .color = color };
	return ll;
}


light_compass light_compass_inst(light_layer south, light_layer east, light_layer north, light_layer west)
{
	light_compass lc = { .se_light_layer = south, .ne_light_layer = east, .nw_light_layer = north, .sw_light_layer = west };
	return lc;
}


light_compass light_compass_inst(layer_color_scheme scheme)
{
	cross_direction se_cross = { 1, SOUTH };
	light_line_pos se_inner = light_line_pos_inst(SE_INNER_LOWER, SE_INNER_UPPER);
	light_line_pos se_middle = light_line_pos_inst(SE_MIDDLE_LOWER, SE_MIDDLE_UPPER);
	light_line_pos se_outer = light_line_pos_inst(SE_OUTER_LOWER, SE_OUTER_UPPER);

	cross_direction ne_cross = { 1, EAST };
	light_line_pos ne_inner = light_line_pos_inst(NE_INNER_LOWER, NE_INNER_UPPER);
	light_line_pos ne_middle = light_line_pos_inst(NE_MIDDLE_LOWER, NE_MIDDLE_UPPER);
	light_line_pos ne_outer = light_line_pos_inst(NE_OUTER_LOWER, NE_OUTER_UPPER);

	cross_direction nw_cross = { 0, NORTH };
	light_line_pos nw_inner = light_line_pos_inst(NW_INNER_LOWER, NW_INNER_UPPER);
	light_line_pos nw_middle = light_line_pos_inst(NW_MIDDLE_LOWER, NW_MIDDLE_UPPER);
	light_line_pos nw_outer = light_line_pos_inst(NW_OUTER_LOWER, NW_OUTER_UPPER);

	cross_direction sw_cross = { 0, WEST };
	light_line_pos sw_inner = light_line_pos_inst(SW_INNER_LOWER, SW_INNER_UPPER);
	light_line_pos sw_middle = light_line_pos_inst(SW_MIDDLE_LOWER, SW_MIDDLE_UPPER);
	light_line_pos sw_outer = light_line_pos_inst(SW_OUTER_LOWER, SW_OUTER_UPPER);

	light_layer se_layer = light_layer_inst(se_cross, se_inner, se_middle, se_outer, scheme.se_color);
	light_layer ne_layer = light_layer_inst(ne_cross, ne_inner, ne_middle, ne_outer, scheme.ne_color);
	light_layer nw_layer = light_layer_inst(nw_cross, nw_inner, nw_middle, nw_outer, scheme.nw_color);
	light_layer sw_layer = light_layer_inst(sw_cross, sw_inner, sw_middle, sw_outer, scheme.sw_color);

	light_compass compass = light_compass_inst(se_layer, ne_layer, nw_layer, sw_layer);
	return compass;
}


int incr_towards_center(int pos)
{
	return pos += 1;
}

int decr_towards_center(int pos)
{
	return pos -= 1;
}

int spiral_show_light_incr(int pos, int color)
{
	light_turn(color, pos);
	get_leds()->show();
	delay(minimal_delay);
	return incr_towards_center(pos);
}

int spiral_show_light_decr(int pos, int color)
{
	light_turn(color, pos);
	get_leds()->show();
	delay(minimal_delay);
	return decr_towards_center(pos);
}

int cross_incr(cross_direction cross, int color)
{
	return cross.should_incr == 1 ? spiral_show_light_incr(cross.pos, color) : spiral_show_light_decr(cross.pos, color);
}

int incr_line(int absolute_pos, int max_size, int line_pos, int color)
{
	return absolute_pos <= max_size ? spiral_show_light_incr(line_pos, color) : 0;
}

int decr_line(int absolute_pos, int max_size, int line_pos, int color)
{
	return absolute_pos <= max_size ? spiral_show_light_decr(line_pos, color) : 0;
}


light_layer light_display_layer(light_layer ll, int absolute_pos)
{

	int inner_max = (INNER_DIRECTION_SIZE / 2) + 1;
	int middle_max = MIDDLE_DIRECTION_SIZE / 2;
	int outer_max = (OUTER_DIRECTION_SIZE / 2); //note: SE only has 2 clicks

	ll.cross.pos = cross_incr(ll.cross, ll.color);
	ll.inner.pos_lower = incr_line(absolute_pos, inner_max, ll.inner.pos_lower, ll.color);
	ll.middle.pos_lower = incr_line(absolute_pos, middle_max, ll.middle.pos_lower, ll.color);
	ll.outer.pos_lower = incr_line(absolute_pos, outer_max, ll.outer.pos_lower, ll.color);

	ll.outer.pos_upper = decr_line(absolute_pos, outer_max, ll.outer.pos_upper, ll.color);
	ll.middle.pos_upper = decr_line(absolute_pos, middle_max, ll.middle.pos_upper, ll.color);
	ll.inner.pos_upper = decr_line(absolute_pos, inner_max, ll.inner.pos_upper, ll.color);
	return ll;
}


//absolute position has to change everything
void spiral_figure(light_compass lc)
{
	for (int i = 0; i < CROSS_DIRECTION_SIZE + 1; i++)
	{
		lc.se_light_layer = light_display_layer(lc.se_light_layer, i);
		lc.ne_light_layer = light_display_layer(lc.ne_light_layer, i);
		lc.nw_light_layer = light_display_layer(lc.nw_light_layer, i);
		lc.sw_light_layer = light_display_layer(lc.sw_light_layer, i);
	}

}


void turn_off_lights()
{
	for (int i = 0; i < 192; i++)
	{		
		get_leds()->setPixelColor(i, 0);
	}
	get_leds()->setPixelColor(SOUTH, std_orange);
	get_leds()->setPixelColor(EAST, std_green);
	get_leds()->setPixelColor(NORTH, std_yellow);
	get_leds()->setPixelColor(WEST, std_pink);
}

int standby_spiral(Adafruit_NeoPixel *leds, BUTTONS *buttons)
{
	layer_color_scheme turn_off = { .se_color = 0, .ne_color = 0, .nw_color = 0, .sw_color = 0 };
	layer_color_scheme turn_on = { .se_color = std_orange, .ne_color = std_green, .nw_color = std_yellow, .sw_color = std_pink };

	if (!setjmp(buf))
	{
		set_leds(leds);

		light_compass lc = light_compass_inst(turn_on);
		light_compass lc_off = light_compass_inst(turn_off);

		spiral_figure(lc);
		spiral_figure(lc_off);
		delay(PAUSE_SCREEN_LIGHT_DELAY_MS);
		get_leds()->show();
	}
	else {
		turn_off_lights();
		get_leds()->show();
		return 0  ;
	}

}