#include "test_board_adafruit.h"

void unit_test_buttons()
{
	for (int i = 0; i < 50; i++)
	{
		get_buttons()->button_update(100);
		delay(101);
	}
}

void unit_test_jump_led(int offset)
{
	for (int i = 0; i < NUM_LEDS; i += offset)
	{
		get_leds()->setPixelColor(i, get_leds()->Color(0, 255, 0));

		delay(flow_slow);
		get_leds()->show();
	}
}


void unit_test_cross()
{
	for (int i = 0; i < 11; i++) {
		get_leds()->setPixelColor(SOUTH_UPPER - i, get_leds()->Color(0, 255, 0));
		get_leds()->setPixelColor(NORTH_LOWER + i, get_leds()->Color(0, 255, 0));
		get_leds()->setPixelColor(EAST_UPPER - i, get_leds()->Color(0, 255, 0));
		get_leds()->setPixelColor(WEST_LOWER + i, get_leds()->Color(0, 255, 0));

		delay(20);
		get_leds()->show();
	}

	for (int k = 0; k < 11; k++) {
		get_leds()->setPixelColor(SOUTH_UPPER - k, get_leds()->Color(0, 0, 0));
		get_leds()->setPixelColor(NORTH_LOWER + k, get_leds()->Color(0, 0, 0));
		get_leds()->setPixelColor(EAST_UPPER - k, get_leds()->Color(0, 0, 0));
		get_leds()->setPixelColor(WEST_LOWER + k, get_leds()->Color(0, 0, 0));

		delay(flow_slow);
		get_leds()->show();
	}

	for (int j = 10; j >= 0; j--) {
		get_leds()->setPixelColor(SOUTH_UPPER - j, get_leds()->Color(0, 255, 0));
		get_leds()->setPixelColor(NORTH_LOWER + j, get_leds()->Color(0, 255, 0));
		get_leds()->setPixelColor(EAST_UPPER - j, get_leds()->Color(0, 255, 0));
		get_leds()->setPixelColor(WEST_LOWER + j, get_leds()->Color(0, 255, 0));

		delay(20);
		get_leds()->show();
	}
}

void unit_test_speed(int mili_speed)
{
	for (int x = 0; x < NUM_LEDS; x++) {
		get_leds()->setPixelColor(x, get_leds()->Color(0, 255, 0));
		delay(20);
		get_leds()->show();
	}
}


void unit_test_runpixels()
{
	for (int x = 0; x < 11; x++) {

		delay(300);
		get_leds()->setPixelColor(10 - x, get_leds()->Color(0xE8, 0xF1, 0x24));

		delay(300);
		get_leds()->setPixelColor(11 + x, get_leds()->Color(0xF0, 0x33, 0x16));

		delay(30);
		get_leds()->setPixelColor(32 - x, get_leds()->Color(0x25, 0xE6, 0x25));

		delay(30);
		get_leds()->setPixelColor(33 + x, get_leds()->Color(0xEE, 0x24, 0xF1));

		get_leds()->show();

	}
}