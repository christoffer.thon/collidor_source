#ifndef CENTER_CONTROL_H
#define CENTER_CONTROL_H

#include "current_hardware_version.h"
#include "Arduino.h"

typedef unsigned char byte; //for some reason we cannot 

enum class Center : unsigned long long
{
	SW_NE = 0b1000000010000000, //PIN-7 & PIN-15
	SE_NW = 0b10000000100000000000000000000000, //pin-23 & pin-31
	SINGLE = 0b10000000100000010000000010000000, //do not use this for now
};


class CENTER_PIECE
{
private:
	byte new_center;
	byte reading;
	byte old_reading;
	Center const_c;
	bool has_moved;
public:

	CENTER_PIECE();

	void update();

	bool is_se();

	bool is_nw();

	bool center_exists();

	int rotation_pos(); //unimplemented

	String center_alignment(); //unimplemented

	String rotation_direction(); //unimplemented

	//figure out a way to see if rotation works
};

#endif // !CENTER_CONTROL_H

