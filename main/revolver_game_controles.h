#ifndef _REVOLVER_GAME_CONTROLS_H
#define _REVOLVER_GAME_CONTROLS_H
/*
   To do:
   generate random particles from centerPin lanes
   move particles at centerPin as centerPin rotates
   Løft når partikel er rød
*/

int row1[] = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67};
int row2[] = {67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86};
int row3[] = {86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105};
int row4[] = {105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124};

CRGB row_color1 = 0xFF0000;
CRGB row_color2 = 0x9000FF;
CRGB row_color3 = 0xEBBB1A;
CRGB row_color4 = 0x1AEB4D;

CRGB row1_colors[19] = {};
CRGB row2_colors[19] = {};
CRGB row3_colors[19] = {};
CRGB row4_colors[19] = {};

bool revolver_init_flag = 1;

void game_3_init() {

  if (revolver_init_flag) {

    Serial.println("init game 3");

    // make start animation here
    // FastLED.show();

    for (int x = 0; x < 18; x++) {

      int random1 = random(0, 4);
      int random2 = random(0, 4);
      int random3 = random(0, 4);
      int random4 = random(0, 4);

      if (random1 == 0) {
        row1_colors[x] = row_color1;
      }
      else if (random1 == 1) {
        row1_colors[x] = row_color2;
      }
      else if (random1 == 2) {
        row1_colors[x] = row_color3;
      }
      else if (random1 == 3) {
        row1_colors[x] = row_color4;
      }

      if (random2 == 0) {
        row2_colors[x] = row_color1;
      }
      else if (random2 == 1) {
        row2_colors[x] = row_color2;
      }
      else if (random2 == 2) {
        row2_colors[x] = row_color3;
      }
      else if (random2 == 3) {
        row2_colors[x] = row_color4;
      }

      if (random3 == 0) {
        row3_colors[x] = row_color1;
      }
      else if (random3 == 1) {
        row3_colors[x] = row_color2;
      }
      else if (random3 == 2) {
        row3_colors[x] = row_color3;
      }
      else if (random3 == 3) {
        row3_colors[x] = row_color4;
      }

      if (random4 == 0) {
        row4_colors[x] = row_color1;
      }
      else if (random4 == 1) {
        row4_colors[x] = row_color2;
      }
      else if (random4 == 2) {
        row4_colors[x] = row_color3;
      }
      else if (random4 == 3) {
        row4_colors[x] = row_color4;
      }
    }

    for (int x = 0; x <= 19; x++) {
      leds[row1[x]] = row1_colors[x];
      leds[row2[x]] = row2_colors[x];
      leds[row3[x]] = row3_colors[x];
      leds[row4[x]] = row4_colors[x];
    }

  }
  revolver_init_flag = 0;
}


int revolver_colors_pos[] = { 12, 36, 11, 35 };
int oldRotation_pos;

void revolver_targets() {

  Serial.println(centerPin.rotation_pos());

  if (centerPin.rotation_pos() > oldRotation_pos) {

    leds[revolver_colors_pos[0]] = row_color1;
    leds[revolver_colors_pos[1]] = row_color2;
    leds[revolver_colors_pos[2]] = row_color3;
    leds[revolver_colors_pos[3]] = row_color4;

    // vibrate(1);

    oldRotation_pos = centerPin.rotation_pos();

  }
  else if (centerPin.rotation_pos() < oldRotation_pos) {

    leds[revolver_colors_pos[0]] = row_color1;
    leds[revolver_colors_pos[1]] = row_color2;
    leds[revolver_colors_pos[2]] = row_color3;
    leds[revolver_colors_pos[3]] = row_color4;

    // vibrate(1);
    oldRotation_pos = centerPin.rotation_pos();

  }
}

int revolver_particle_speed1 = 40, revolver_particle_speed2 = 60, revolver_particle_speed3 = 80, revolver_particle_speed4 = 100;
unsigned int revolver_particle_pos1, revolver_particle_pos2, revolver_particle_pos3, revolver_particle_pos4;

int revolver_lane_1[] = {0, 11};
int revolver_lane_2[] = {24, 35};
int revolver_lane_3[] = {12, 24};
int revolver_lane_4[] = {35, 48};

unsigned int revolver_particle_1_incrementer;
unsigned int revolver_particle_2_incrementer;
unsigned int revolver_particle_3_incrementer;
unsigned int revolver_particle_4_incrementer;

int revolver_random_seed;

void fire_particle1(int lane_values[2]) {

  if (revolver_particle_1_incrementer < lane_values[1]) {
    leds[revolver_particle_1_incrementer] = row1_colors[0];
    revolver_particle_1_incrementer++;
  }
}

void revolver_color_row_update() {

  for (int x = 0; x <= 19; x++) {
    leds[row1[x]] = row1_colors[x];
    leds[row2[x]] = row2_colors[x];
    leds[row3[x]] = row3_colors[x];
    leds[row4[x]] = row4_colors[x];
  }

  //fire particle1 at lane1 with speed1 (test)
  EVERY_N_MILLISECONDS(revolver_particle_speed1)
  {
    fire_particle1(revolver_lane_1);
  }

  EVERY_N_MILLISECONDS(revolver_particle_speed2)
  {

  }

  EVERY_N_MILLISECONDS(revolver_particle_speed3)
  {

  }

  EVERY_N_MILLISECONDS(revolver_particle_speed4)
  {

  }


}

#endif // !_REVOLVER_GAME_CONTROLS_H