#include "candycrush.h"

sequantial_color* sc1 = nullptr;
sequantial_color* sc2 = nullptr;
sequantial_colors* collidor_sc_colors = nullptr;

vibrator g_vibrator;

char is_pressed = 0;
candycrush_state state = candycrush_state::running;

void foo(String whatever, int value)
{
	Serial.print(whatever);
	Serial.println(value);
}


//incl for both ranges
void random_color_range(int from, int to)
{
	for (int i = from; i <= to; i++)
	{
		set_random_color(i);
	}
}

int check_success_upwards(int pos, int end)
{
	int sucesses = 1;
	for (int i = pos; i < end; i++)
	{
		if (get_color(i) != get_color(i + 1))
		{
			//add position here
			return sucesses;
		}
		sucesses++;
	}
	return sucesses;
}

sequantial_color sc_check_success_upwards(int pos, sequantial_color sc)
{
	for (int i = pos; i <= sc.line->pos_upper; i++)
	{
		sc.add_pos(i);
		if (get_color(i) != get_color(i + 1))
		{
			break;
		}
	}
	return sc;
}

int check_success_downwards(int pos, int end)
{
	int sucesses = 1;
	for (int i = end; i > pos; i--)
	{
		if (get_color(i) != get_color(i - 1))
		{
			return sucesses;
		}
		sucesses++;
	}
	return sucesses;
}

sequantial_color sc_check_success_downwards(int pos, sequantial_color sc)
{
	//foo("pos downwards: ", pos);
	//foo("sc_lower: ", sc.line->pos_lower);
	for (int i = pos; i >= sc.line->pos_lower; i--)
	{
		sc.add_pos(i);
		if (get_color(i) != get_color(i - 1))
		{
			break;
		}
	}
	return sc;
}

int check_success_between(int pos, int min, int max)
{
	int successes = 1;
	if (pos > min)
	{
		if (get_color(pos) == get_color(pos - 1))
			successes++;
	}
	if (pos < max)
	{
		if (get_color(pos) == get_color(pos + 1))
			successes++;
	}
	return successes;
}

sequantial_color sc_check_success_between(int pos, sequantial_color sc)
{
	if (pos > sc.line->pos_lower)
	{
		if (get_color(pos) == get_color(pos - 1))
			sc.add_pos(pos - 1);
	}
	if (pos < sc.line->pos_upper)
	{
		if (get_color(pos) == get_color(pos + 1))
			sc.add_pos(pos + 1);
	}
}



void line_shuffle_upwards(int start, int end, int sucesses_row)
{
	for (int i = start; i < end; i++)
	{
		//performance can be improved	
		int res = check_success_upwards(i, end);
		while (res >= sucesses_row)
		{
			res = check_success_upwards(i, end);
			random_color_range(i, end);
		}
	}
}

void shuffle_downwards(int start, int end, int sucesses_row)
{
	int retry = 1;
	while (retry)
	{
		if (check_success_downwards(end, start) >= sucesses_row)
		{
			random_color_range(start, end);
			retry = 0;
		}
	}
}

void shuffle_compass()
{
	line_shuffle_upwards(SOUTH, CROSS_DIRECTION_SIZE, MIN_SUCCESSES);
	line_shuffle_upwards(NORTH - CROSS_DIRECTION_SIZE, NORTH, MIN_SUCCESSES);
	line_shuffle_upwards(EAST, EAST + CROSS_DIRECTION_SIZE, MIN_SUCCESSES);
	line_shuffle_upwards(WEST - CROSS_DIRECTION_SIZE, WEST, MIN_SUCCESSES);

	line_shuffle_upwards(NE_INNER_LOWER, NE_INNER_UPPER, MIN_SUCCESSES);
	line_shuffle_upwards(NE_MIDDLE_LOWER, NE_MIDDLE_UPPER, MIN_SUCCESSES); //three in a row here
	line_shuffle_upwards(NE_OUTER_LOWER, NE_OUTER_UPPER, MIN_SUCCESSES);

	line_shuffle_upwards(SE_INNER_LOWER, SE_INNER_UPPER, MIN_SUCCESSES);
	line_shuffle_upwards(SE_MIDDLE_LOWER, SE_MIDDLE_UPPER, MIN_SUCCESSES);
	line_shuffle_upwards(SE_OUTER_LOWER, SE_OUTER_UPPER, MIN_SUCCESSES);

	line_shuffle_upwards(NW_INNER_LOWER, NW_INNER_UPPER, MIN_SUCCESSES);
	line_shuffle_upwards(NW_MIDDLE_LOWER, NW_MIDDLE_UPPER, MIN_SUCCESSES);
	line_shuffle_upwards(NW_OUTER_LOWER, NW_OUTER_UPPER, MIN_SUCCESSES); //three in a row here

	line_shuffle_upwards(SW_INNER_LOWER, SW_INNER_UPPER, MIN_SUCCESSES);
	line_shuffle_upwards(SW_MIDDLE_LOWER, SW_MIDDLE_UPPER, MIN_SUCCESSES); //three in a row here
	line_shuffle_upwards(SW_OUTER_LOWER, SW_OUTER_UPPER, MIN_SUCCESSES);
}


void light_up_compass()
{
	random_color_range(SOUTH, MAX_SIZE);
	shuffle_compass();

	get_leds()->show();
}

void candycrush::init()
{
	//light_up_compass();
	get_leds()->setPixelColor(0, std_yellow);
	get_leds()->setPixelColor(SW_INNER_LOWER, std_green);
	//random_color_range(SOUTH, SOUTH_UPPER);
	//line_shuffle_upwards(SOUTH, SOUTH_UPPER, MIN_SUCCESSES);
	//random_color_range(SW_INNER_LOWER, SW_INNER_UPPER);
	//line_shuffle_upwards(SW_INNER_LOWER, SW_INNER_UPPER, MIN_SUCCESSES);
	get_leds()->show();

	//state_container	
	sc1 = (sequantial_color*)malloc(sizeof(sequantial_color));
	sc1->max_size = MAX_SUCCESSES;
	sc2 = (sequantial_color*)malloc(sizeof(sequantial_color));
	sc2->max_size = MAX_SUCCESSES;

	sc1->sequantial_positions = (int*)malloc(sizeof(int) * MAX_SUCCESSES);
	sc2->sequantial_positions = (int*)malloc(sizeof(int) * MAX_SUCCESSES);

	collidor_sc_colors = (sequantial_colors*)malloc(sizeof(sequantial_colors));
	collidor_sc_colors->color_lines = (sequantial_color*)malloc(sizeof(sequantial_color) * MAX_LANES);
	collidor_sc_colors->max_size = MAX_LANES;

	//g_vibrator = vibrator();
}



positions candycrush::register_buttons(){
	positions p(-1, -1);
	for (int i = 0; i < AMOUNT_OF_BUTTONS; i++)
	{
		button_map bmap = button_mapping[i];
		Button selected = bmap.button;
		if (input->is_down(selected))
		{
			//Serial.println("inside isdown");
			if (selected != prev_map.button)
			{
				//Serial.print("inside is_down:"); Serial.println(millis());
				if (prev_map.button == Button::None)
				{
					prev_map.button = selected;
					prev_map.led_pos = bmap.led_pos;
					continue;
				}

				//swap in here
				auto color1 = get_color(bmap.led_pos);
				auto color2 = get_color(prev_map.led_pos);
				light_move_crgb(bmap.led_pos, color2);
				light_move_crgb(prev_map.led_pos, color1);

				g_vibrator.add_job(SMALL_VIBRATION, 200);
				g_vibrator.update();
				//Serial.println("moving hardcoded colors");			
				get_leds()->show();
				//Serial.println("-----resets buttons----");

				//clear buttons
				prev_map.button = Button::None;
				bmap.button = Button::None;
				//delay(HALF_SECOND);
				//Serial.println("returning");
				return positions(bmap.led_pos, prev_map.led_pos);
			}
			//wants to reset button after waiting period
			//wants to blink	
		}
	}

	return p;
}

void candycrush::successes_move_row_vertical()
{
	//knows line_pos1 has 2 blinks higher and of same color
	for (int i = 0; i < collidor_sc_colors->size; i++)
	{
		//sequantial_color c = results.color_lines[i];//failed here?
		sequantial_color c = collidor_sc_colors->color_lines[i];
		if (c.size >= MIN_SUCCESSES)
		{
			blink(c.sequantial_positions, c.size, HALF_SECOND / 3, COLOR_BLINKS - 1);

			c.sqp_change_light(0, HALF_SECOND / 8);
			//Serial.println("done, finished, etc.");
			c.move_line_down();
		}
	}
}

bool sequantial_color::color_not_black(color check_color)
{
	return check_color != 0 ? 1 : 0;
}

void sequantial_color::move_towards_lower(int incr)
{
	int end = line->pos_lower + incr;
	for (int j = 0; j < size; j++)
	{
		//move colors 1 step down
		for (int i = line->pos_lower; i <= end; i++)
		{
			//find_next_color
			//CRGB move_color;
			auto move_color = get_color(i);
			//careful not go to another line for first item
			if (i == line->pos_lower)
			{
				continue;
			}
			//move each light 1 down
			light_move_crgb(i - 1, move_color);
			get_leds()->show();
			//FastLED.show();
		}
		//turn of lights at the top
		light_move_crgb(end - j, 0);
		get_leds()->show();
		delay(HALF_SECOND / 8);
	}
}

void sequantial_color::move_towards_upper(int decr)
{
	int end = line->pos_lower + decr;
	for (int i = 0; i < size; i++)
	{
		int towards_upper = 0;
		for (int j = line->pos_upper; j > end; j--)
		{
			//find_next_color
			//CRGB move_color;
			color move_color;
			move_color = get_color(j);

			towards_upper++;
			if (j == line->pos_upper)
			{
				continue;
			}
			light_move_crgb(j + 1, move_color);
			get_leds()->show();
			//FastLED.show();
		}
		//light_move_crgb(line->pos_lower + i, 0);
		light_move_crgb(end + i, 0);
		get_leds()->show();
		delay(HALF_SECOND / 2);
	}
}

char sequantial_color::is_cross(int lower_check)
{
	return lower_check == SOUTH || lower_check == EAST || lower_check == NORTH || lower_check == WEST;
}

void sequantial_color::move_line_down()
{
	//foo("seq_pos0:", sequantial_positions[0]);
	if (sequantial_positions[0] == line->pos_lower)
	{
		//Serial.println("inside pos_lower confirmed");		
		if (is_cross(line->pos_lower))
		{
			int upper_limit = line->pos_upper - line->pos_lower;
			move_towards_lower(upper_limit);
		}
		else {
			int diff = (line->pos_upper - line->pos_lower) / 2;
			//foo("diff:", diff);
			move_towards_lower(diff);
		}
	}
	else if (sequantial_positions[0] == line->pos_upper)
	{
		//Serial.println("line pos_upper confirmed");
		if (is_cross(line->pos_upper))
		{
			move_towards_upper(0);
		}
		else {
			int decr = ((line->pos_upper - line->pos_lower) / 2) + 1;
			move_towards_upper(decr);
		}
	}
	//Serial.println("finished 1 line down");
}

void candycrush::check_success(positions buttons_pos)
{
	////Serial.println("checking success");
	//Serial.print("before;line_pos1:"); Serial.println(buttons_pos.line_pos1);
	//Serial.print("before;line_pos2:"); Serial.println(buttons_pos.line_pos2);
	light_line* line_pos1 = compass::get_compass()->get_line(buttons_pos.pos1);
	light_line* line_pos2 = compass::get_compass()->get_line(buttons_pos.pos2);
	if (line_pos1 == nullptr || line_pos2 == nullptr)
	{
		Serial.println("false positive from button identified"); //should throw exception here
	}

	sc1->line = line_pos1;
	sc1->size = 0;

	sc2->line = line_pos2;
	sc2->size = 0;

	*sc1 = success_vertical(buttons_pos.pos1, *sc1);
	*sc2 = success_vertical(buttons_pos.pos2, *sc2);

	//sequantial_colors sqc;
	//foo("sc2 size: ", sc2.size);
	collidor_sc_colors->size = 0;
	if (sc2->size >= MIN_SUCCESSES)
	{
		collidor_sc_colors->add_color_line(*sc2);
	}

	//foo("sc1 size: ", sc1.size);
	if (sc1->size >= MIN_SUCCESSES)
	{
		//Serial.println("inside min successes");
		collidor_sc_colors->add_color_line(*sc1);
	}

	//Serial.println("finished boundaries");	
}

sequantial_color candycrush::success_vertical(int pos, sequantial_color sc)
{
	//foo("line lower:", sc.line->pos_lower);
	//foo("line upper:", sc.line->pos_upper);
	//foo("pos:", pos);
	sc = sc_check_success_upwards(pos, sc);
	sc = sc_check_success_downwards(pos, sc);
	//sc = sc_check_success_between(pos, sc);	
	return sc;
}


void candycrush::tick()
{
	//works
	should_reset_game();

	positions buttons_pos = register_buttons();
	//g_vibrator.update();

	buttons_pos.pos1 = SOUTH;
	buttons_pos.pos2 = SW_INNER_UPPER;
	delay(1000);

	if (buttons_pos.pos1 >= 0 && buttons_pos.pos2 >= 0)
	{
		candycrush::check_success(buttons_pos);
		if (collidor_sc_colors->size > 0)
		{
			candycrush::successes_move_row_vertical();
		}

		collidor_sc_colors->reset();
	}

}

void candycrush::shutdown()
{
	free(sc1->sequantial_positions);
	free(sc1);

	free(sc2->sequantial_positions);
	free(sc2);

	free(collidor_sc_colors->color_lines);
	free(collidor_sc_colors);

	Serial.println("calling shutdown");
}


char sequantial_color::has_position(int position)
{
	for (int i = 0; i <= size; i++)
	{
		if (sequantial_positions[i] == position)
		{
			//Serial.println("has_position");
			return 1;
		}
	}
	return 0;
}

void sequantial_color::add_pos(int pos)
{
	if (!has_position(pos))
	{
		sequantial_positions[size++] = pos;
	}

}

void sequantial_color::reset_positions()
{
	size = 0;
}

void sequantial_colors::add_color_line(sequantial_color color)
{
	//TODO: add !has_color_line
	color_lines[size++] = color;
}

void sequantial_color::sqp_change_light(color color, int miliseconds)
{
	//foo("size:", size);	
	for (int i = size - 1; i >= 0; i--)
	{
		light_move_crgb(sequantial_positions[i], color);
		delay(miliseconds);
		get_leds()->show();
	}
	//Serial.println("finished");
}


void sequantial_colors::reset()
{	
	size = 0;
}

void candycrush::should_reset_game()
{
	bool center_off = center.center_exists();

	if (state == candycrush_state::init)
	{
		foo("state init: ", (int)state);
		init();
		state = candycrush_state::running;
	}
	else if(state == candycrush_state::running)
	{
		if (center_off)
		{
			foo("state running: ", (int)state);
			compass::get_compass()->turn_off(SOUTH, SW_OUTER_UPPER + 1, 0);
			state = candycrush_state::shutdown;
			shutdown();
		}
	}
	else if(state == candycrush_state::shutdown)
	{
		if (!center_off)
		{
			foo("state shutdown: ", (int)state);
			state = candycrush_state::init;
		}
		return;
	}
}
