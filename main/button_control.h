#ifndef BUTTON_CONTROL_H
#define BUTTON_CONTROL_H

#include <Arduino.h>
//#include "hardwarepins_vemos_mini_d1_esp8266_version3.h"
//#include "hardwarepins_generic_esp8266.h"
//#include "board_vemos_mini_d1_esp8266_version3.h"
#include "current_hardware_version.h"
#include <string>


#define BUTTON_MAPPING_SIZE 27

//each int is an index of a bit-32, read from lowest-highest using MSB
enum class Button : int
{
	None		=		-1,
	SouthZero	=		27, 
	SouthOne	=		28,
	SouthTwo	=		29,
	SouthThree	=		30,
	SouthFour	=		16,
	SouthFive	=		17,
	SouthSix	=		18,
	EastZero	=		19, //doesn't work
	EastOne		=		20,
	EastTwo		=		21,
	EastThree	=		22,
	EastFour	=		8,
	EastFive	=		9,
	EastSix		=		10,
	NorthZero	=		11,
	NorthOne	=		12,
	NorthTwo	=		13,
	NorthThree	=		14,
	NorthFour	=		0,
	NorthFive	=		1,
	NorthSix	=		2,
	WestZero	=		3,
	WestOne		=		4,
	WestTwo		=		5,
	WestThree	=		6,
	WestFour	=		24,
	WestFive	=		25,
	WestSix		=		26, //doesn't work
};


typedef struct
{
	Button button;
	int led_pos;
	std::string name;
}button_map;


//size: 27, none is not included 
static button_map button_mapping[] =
{
	{Button::SouthZero, SOUTH, "SouthZero"},
	{Button::SouthOne, SE_INNER_LOWER, "SouthOne"},
	{Button::SouthTwo, SE_MIDDLE_LOWER, "SouthTwo"},
	{Button::SouthThree, SE_OUTER_LOWER, "SouthThree"},
	{Button::SouthFour, SE_OUTER_UPPER, "SouthFour"},
	{Button::SouthFive, SE_MIDDLE_UPPER, "SouthFive"},
	{Button::SouthSix, SE_INNER_UPPER, "SouthSix"},
	{Button::EastZero, EAST, "EastZero"},
	{Button::EastOne, NE_INNER_LOWER, "EastOne"},
	{Button::EastTwo, NE_MIDDLE_LOWER, "EastTwo"},
	{Button::EastThree, NE_OUTER_LOWER, "EastThree"},
	{Button::EastFour, NE_OUTER_UPPER, "EastFour"},
	{Button::EastFive, NE_MIDDLE_UPPER, "EastFive"},
	{Button::EastSix, NE_INNER_UPPER, "EastSix"},
	{Button::NorthZero, NORTH, "NorthZero"},
	{Button::NorthOne, NW_INNER_LOWER, "NorthOne"},
	{Button::NorthTwo, NW_MIDDLE_LOWER, "NorthTwo"},
	{Button::NorthThree, NW_OUTER_LOWER, "NorthThree"},
	{Button::NorthFour, NW_OUTER_UPPER, "NorthFour"},
	{Button::NorthFive, NW_MIDDLE_UPPER, "NorthFive"},
	{Button::NorthSix, NW_INNER_UPPER, "NorthSix"},
	{Button::WestZero, WEST, "WestZero"},
	{Button::WestOne, SW_INNER_LOWER, "WestOne"},
	{Button::WestTwo, SW_MIDDLE_LOWER, "WestTwo"},
	{Button::WestThree, SW_OUTER_LOWER, "WestThree"},
	{Button::WestFour, SW_OUTER_UPPER, "WestFour"},
	{Button::WestFive, SW_MIDDLE_UPPER, "WestFive"},
	{Button::WestSix, SW_INNER_UPPER, "WestSix"}
};


class BUTTONS {

private:
	unsigned long defult_interval;
	unsigned long lastUpdate;
	byte button_states[32];

	String myString;

public:

	BYTES_VAL_T pinValues;
	BYTES_VAL_T oldPinValues;

	BUTTONS(int interval);
	
	/* Dump the list of zones along with their current status.*/
	void display_pin_values();

	void init();

	void button_update(int interval);

	byte button_states_updated();

	bool is_down(Button button);

	bool is_released_simultaneously(Button button) ;

	bool is_pressed_simultaneously(Button button);

	bool is_released(Button button);

	bool is_pressed(Button button);

	button_map* first_press();
};
#endif