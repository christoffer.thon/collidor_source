#ifndef STANDBY_SPIRAL_H
#define STANDBY_SPIRAL_H


#define OUTER_TO_MIDDLE_DIFF 6
#define MIDDLE_TO_INNER_DIFF 3
#define MIDDLE_TO_CROSS_DIFF 2
#define minimal_delay 40

#include <setjmp.h>
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

//#include "standby_spiral.h"
#include "button_control.h"
#include <string.h>
#include "shared_constants.h"
#include "lib_collidor.h"

typedef struct {
  int se_color;
  int ne_color;
  int nw_color;
  int sw_color;
} layer_color_scheme;

typedef struct {
  int pos_lower;
  int pos_upper;
} light_line_pos;

typedef struct {
  char should_incr;
  int pos;
} cross_direction;

typedef struct {
  cross_direction cross;
  light_line_pos inner;
  light_line_pos middle;
  light_line_pos outer;
  int color;
} light_layer;

typedef struct {
  light_layer se_light_layer; //cross: south
  light_layer ne_light_layer; //cross: east
  light_layer nw_light_layer; //cross: north
  light_layer sw_light_layer; //cross: west
} light_compass;


int standby_spiral(Adafruit_NeoPixel *leds, BUTTONS *buttons);
int incr_towards_center(int pos);
int decr_towards_center(int pos);
void spiral_figure(light_compass lc);
void light_turn(int color_scheme, int pos);
int spiral_show_light_incr(int pos, int color);
int spiral_show_light_decr(int pos, int color);
int cross_incr(cross_direction cross, int color);
void light_next(int color_scheme, int pos, int prev_pos);
light_line_pos light_line_pos_inst(int lower, int upper);
light_compass light_compass_inst(layer_color_scheme scheme);
light_layer light_display_layer(light_layer ll, int absolute_pos);
int incr_line(int absolute_pos, int max_size, int line_pos, int color);
int decr_line(int absolute_pos, int max_size, int line_pos, int color);
void circle_layer(int color_scheme, int pos_lower, int pos_upper, int offset);
light_compass light_compass_inst(light_layer south, light_layer east, light_layer north, light_layer west);
light_layer light_layer_inst(cross_direction compass, light_line_pos inner, light_line_pos middle, light_line_pos outer, int color);

#endif // !STANDBY_SPIRAL_H
