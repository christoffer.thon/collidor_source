#include "menu.h"

Menu::Menu(Adafruit_NeoPixel* pixels, BUTTONS* buttons, CENTER_PIECE* center)
{
	this->pixels = pixels;
	this->buttons = buttons;
	this->center = center;
}


void Menu::init()
{
	for (int x = 0; x < 11; x++) {
		pixels->setPixelColor(SOUTH_UPPER - x, std_yellow);
		pixels->setPixelColor(EAST_UPPER - x, std_pink);
		pixels->setPixelColor(NORTH_LOWER + x, std_green);
		pixels->setPixelColor(WEST_LOWER + x, std_orange);

		pixels->show();
		delay(40);


		pixels->setPixelColor(SOUTH_UPPER - x, 0);
		pixels->setPixelColor(EAST_UPPER - x, 0);
		pixels->setPixelColor(NORTH_LOWER + x, 0);
		pixels->setPixelColor(WEST_LOWER + x, 0);

		if (x == 10) {
			//vibrate(1);
		}
	}


	// game mode colors
	pixels->setPixelColor(NORTH, std_green);
	pixels->setPixelColor(EAST, std_pink);
	pixels->setPixelColor(WEST, std_orange);
	pixels->setPixelColor(SOUTH, std_yellow);

	pixels->show();
}

byte Menu::choose_player_amount()
{
	////center_in_place = center->center_exists();

	//rotary_constrain_value = constrain(center->rotation_pos(), 0, 3);
	//Serial.println(rotary_constrain_value);

	////rotary_constrain_value = 1;

	//if (rotary_constrain_value >= 0) {
	//	cleds[9] = 0xF03316;
	//	if (center->center_exists()) {
	//		//delay(10);
	//		return rotary_constrain_value + 1;
	//	}
	//}
	//if (rotary_constrain_value > 0) {
	//	cleds[38] = 0xF03316;
	//	if (center->center_exists()) {
	//		// delay(10);
	//		return rotary_constrain_value + 1;
	//	}
	//}
	//else {		
	//	cleds->fadeToBlackBy(20);
	//}
	//if (rotary_constrain_value > 1) {
	//	cleds[14] = 0xF03316;
	//	if (center->center_exists()) {
	//		//delay(10);
	//		return rotary_constrain_value + 1;
	//	}
	//}
	//else {
	//	cleds->fadeToBlackBy(20);
	//}
	//if (rotary_constrain_value > 2) {
	//	cleds[33] = 0xF03316;
	//	if (center->center_exists()) {
	//		// delay(10);
	//		return rotary_constrain_value + 1;
	//	}
	//}
	//else {
	//	cleds->fadeToBlackBy(20);
	//}

	//FastLED.show();

	//returns 0 as defult
	return 0;
}

game* Menu::choose_mode(button_map* choice)
{
	if (choice == nullptr)
	{
		return nullptr;
	}
	if (Button::SouthZero == choice->button)
	{
		return nullptr;
	}

	if (Button::EastZero == choice->button)
	{
		Serial.println("clicked eastZero");
		button_map prev_map = { Button::None, -1 };
		candycrush* cc = new candycrush(pixels, buttons, prev_map, *center);
		return cc;
	}

	if (Button::NorthZero == choice->button)
	{
		//collidor game here
		return nullptr;
	}

	if (Button::WestZero == choice->button)
	{
		/*
			rubiks_game* rb = new rubiks_game(center, buttons, leds);
			return rb;*/
	}

	return nullptr;
}