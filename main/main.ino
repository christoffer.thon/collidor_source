/* Needs to be compiled for: Generic ESP8266 */
#define ALL_GAMES 4
#define FASTLED_ALLOW_INTERRUPTS 0
#define board_debug 1

#include <Adafruit_NeoPixel.h>
#include "shared_constants.h"

#include "Arduino.h"

#include "current_hardware_version.h"


//#include "help_functions.h"

//#include "center_control.h"
//#include "button_control.h"

#include "test_board_adafruit.h"
//#include "vibration_control.h"

//#include "collidor_game_controles.h"
//#include "revolver_game_controles.h"
//#include "rubiks_game_controles.h"
//#include "overflow_game_controles.h"

//#include "menu.h"
//#include "standby.h"
//#include "standby_spiral.h"
//#include "candycrush.h"
//#include "game.h"
//#include "lib_collidor.h"

Adafruit_NeoPixel leds(NUM_LEDS, DATA_LED, NEO_RGB + NEO_KHZ800);



//String button_states;
//bool center_in_place;
//String center_aligemnt;
//long pos_holder;

//game* choose_mode(Button choice);

//byte current_game = 0;
//byte players = 0;
//
//bool game_mode_flag = LOW;
//bool players_flag = LOW;
//
//unsigned int fadeOutCounter;
//bool fadeOutFlag = 0;
//game* c_game = nullptr;

bool game_chosen = false;

button_map total_games[ALL_GAMES];

//each initialization should use a singleton pattern instead
BUTTONS buttons = BUTTONS(1);
CENTER_PIECE center = CENTER_PIECE();	
//Menu menu = Menu(&leds, &buttons, &center);
char menu_shown = 0;

//work on buttons: dbut, figure out why bits are reversed compared to ESP12eSetup.ino
void setup() {
	Serial.begin(115200);
	delay(500); //neccessary to activate setup
	randomSeed(analogRead(0));	

	//read_shift_regs();

	leds.begin();
	leds.setBrightness(np_brightness);
	leds.clear();

	set_leds(&leds);
	//set_buttons(&buttons);
	//set_center(&center);

}

//int idle = 1;
//int time_start_figure = millis() <-- set for figures

#if !defined(board_debug)
void loop() {
		
	buttons.button_update(1);
	center.update();

	leds.setPixelColor(0, 255, 0, 255);
	leds.show();
	/*if (menu_shown == 0)
	{
		menu.init();
		menu_shown = 1;
	}*/
		
	//vibrate_update();
	////delay(1000);

	if (c_game != nullptr)
	{
		//Serial.println("calling tick from !nullptr");
		//c_game->tick();
	}
	else
	{
		if (!game_chosen)
		{
			//button_map* choice = buttons.first_press();
			//c_game = menu.choose_mode(choice);
			Serial.println("starting up candycrush");
			button_map prev_map = { Button::None, -1 };
			c_game = new candycrush(get_leds(), get_buttons(), prev_map, *(get_center())); //center should be pointer
			if (c_game != nullptr)
			{
				c_game->init();
				game_chosen = 1;
			}
		}
		//if (idle && millis() > )
		//{
		//	//idle = standby_spiral(&leds);
		//	//standby_animation();
		//}
	}
	return;
} 
#else

void loop() {
	get_leds()->clear(); // Set all pixel colors to 'off'
	get_leds()->setPixelColor(0, std_yellow);
	get_leds()->setPixelColor(3, std_orange);
	get_leds()->show();
	delay(1000);
	String command = "nochoice";
	//Serial.println("Printing to Serial");
	if (Serial.available())
	{
		command = Serial.readStringUntil('\n');
	}

	// The first NeoPixel in a strand is #0, second is 1, all the way up
	// to the count of pixels minus one.

	get_leds()->setBrightness(10);

	if (command == "runpixels")
	{
		Serial.println("running runpixels");
		unit_test_runpixels();
	}
	if (command == "pixelspeed")
	{
		Serial.println("running pixelspeed");

		unit_test_speed(20);
	}
	if (command == "cross")
	{
		Serial.println("running cross");
		unit_test_cross();
	}
	if (command == "jump")
	{
		//must increase brightness to 255
		Serial.println("running jump");
		unit_test_jump_led(2);
	}
	if (command == "showb")
	{
		Serial.println("running showb");
		unit_test_buttons();
	}

	if (command == "digread")
	{
		buttons.button_update(0);
		buttons.display_pin_values();
	}

	if (command == "showbut") 
	{
		//should ask for input from each button
		buttons.button_update(0);				

		for (int i = 0; i < 27; i++)
		{
			button_map m = button_mapping[i];
			char output = bitRead(buttons.pinValues, (int)m.button);
			if (output == 1)
			{
				Serial.print("Button Registered: ");
				Serial.println(m.name.c_str());
				//Serial.print(":");
				//Serial.println(m.button);
				buttons.display_pin_values();
			}
			//build the string 
		}

		if (buttons.is_down(Button::SouthZero))
		{
			Serial.println("pressed south button");
		}
		else {
			Serial.println("no match registeret");
		}
	}

	if (command == "hall")
	{

	}
}
#endif








