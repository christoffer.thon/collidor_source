#ifndef HARDWARE_PINS_H
#define HARDWARE_PINS_H

#include "Arduino.h"

#define BYTES_VAL_T unsigned long

/* Numb shift register chips are daisy-chained.*/
#define NUMBER_OF_SHIFT_CHIPS   4
/* Width of data (how many ext on each lines).*/
#define DATA_WIDTH   NUMBER_OF_SHIFT_CHIPS * 8
/* Width of pulse to trigger the shift register to read and latch.*/
//#define PULSE_WIDTH_USEC   15
#define PULSE_WIDTH_USEC   200
/* Optional delay between shift register reads.*/
#define POLL_DELAY_MSEC   2

constexpr int ploadPin = 2;
constexpr int miso = 14; //input from buttons & hall sensors. //SN74HC165N.
constexpr int clockPin = 4; // D2
constexpr int centerPin = 5; // D1

constexpr int DATA_LED = 12; //LEDS

BYTES_VAL_T read_shift_regs();

#endif // !HARDWARE_PINS_H