#ifndef TEST_LIBRARY_H
#define TEST_LIBRARY_H

#include "shared_constants.h"
#include "current_hardware_version.h"
#include "lib_collidor.h"
#include "button_control.h"

void unit_test_jump_led(int offset);
void unit_test_cross();
void unit_test_speed(int mili_speed);
void unit_test_runpixels();
void unit_test_buttons();
#endif // !1
