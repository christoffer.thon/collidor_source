#include "button_control.h"

BUTTONS::BUTTONS(int interval) {
	this->defult_interval = interval;
	init();
}

/* Dump the list of zones along with their current status.*/
void BUTTONS::display_pin_values()
{
	//Serial.println("new pinValues");
	for (int j = 0; j < DATA_WIDTH; j++)
	{
		char output = bitRead(pinValues, j);
		
		if ((j != 0) && (j % 8 == 0))
		{
			Serial.print("\t");
		}
		Serial.print(output, BIN);
	}
	Serial.println();
	/*for (int i = 0; i < DATA_WIDTH; i++)
	{
		char output = bitRead(oldPinValues, i);
		if ((i != 0) && (i % 8 == 0))
		{
			Serial.print("\t");
		}
		Serial.print(output, BIN);
	}*/
	Serial.print("\r\n");
}

void BUTTONS::init() {

	//delay(100);

	/* Initialize digital pins...*/
	pinMode(ploadPin, OUTPUT);
	pinMode(clockPin, OUTPUT);
	pinMode(miso, INPUT);

	//delay(5);

	digitalWrite(clockPin, LOW);
	digitalWrite(ploadPin, HIGH);

	/* Read in and display the pin states at startup.*/
	pinValues = read_shift_regs();
	//display_pin_values();
	oldPinValues = pinValues;

	lastUpdate = millis();
	//button_update(defult_interval);
}

void BUTTONS::button_update(int interval) {

	if ((millis() - lastUpdate) > interval) // time to update
	{
		//Serial.println("updating buttons");
		lastUpdate = millis();

		/* Read the state of all zones.*/
		pinValues = read_shift_regs();

		//Serial.println("new");
		//Serial.println(display_pin_values(0));
		String old_pin_display = "";
		for (int i = 0; i < DATA_WIDTH; i++)
		{
			if ((oldPinValues >> i) & 1)
			{
				old_pin_display.concat("1");
			}
			else
			{
				old_pin_display.concat("0");
			}
		}
		//Serial.println("old");
		//Serial.println(old_pin_display);

		//Serial.println((~pinValues), BIN);
		/* If there was a chage in state, display which ones changed.*/
		if (pinValues != oldPinValues)
		{
			//Serial.print("*Pin value change detected*\r\n");
			//display_pin_values(interval);
			oldPinValues = pinValues;
			//Serial.println();
		}
	}
}

byte BUTTONS::button_states_updated() {
	button_update(defult_interval);
	for (int i = 0; i < DATA_WIDTH; i++) {
		return (button_states[i]);
	}
}

bool BUTTONS::is_down(Button button)
{
	return bitRead(pinValues, (int)button);
	//return (pinValues & (long)button) == (long)button;
}

bool BUTTONS::is_released_simultaneously(Button button)
{
	char bit_val = bitRead(oldPinValues, (int)button);

	return !is_down(button) && (oldPinValues & bit_val) != 1;
	//return !is_down(button) && (oldPinValues & (long)button) != (long)button;
}

bool BUTTONS::is_pressed_simultaneously(Button button)
{
	char bit_val = bitRead(oldPinValues, (int)button);
	return is_down(button) && bit_val == 0;
	//return is_down(button) && (oldPinValues & (long)button) == 0;
}

bool BUTTONS::is_released(Button button)
{
	char bit_val = bitRead(oldPinValues, (int)button);
	return !is_down(button) && bit_val != 0;
	//return !is_down(button) && (oldPinValues & (long)button) != 0;
}

bool BUTTONS::is_pressed(Button button)
{
	char bit_val = bitRead(oldPinValues, (int)button);
	return is_down(button) && bit_val != 1;
	//return is_down(button) && (oldPinValues & (long)button) != (long)button;
}

button_map* BUTTONS::first_press()
{
	for (int i = 0; i < BUTTON_MAPPING_SIZE; i++)
	{
		if (is_down(button_mapping[i].button))
		{
			return &button_mapping[i];
		}
	}
	return nullptr;
}