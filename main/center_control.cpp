#pragma once
#include "center_control.h"

CENTER_PIECE::CENTER_PIECE()
{
	pinMode(centerPin, INPUT_PULLUP);
	new_center = digitalRead(centerPin);
	reading = read_shift_regs();
	has_moved = false;
};

void CENTER_PIECE::update()
{
	new_center = digitalRead(centerPin);

	reading = read_shift_regs();
	if (reading != old_reading && reading != (long)Center::SINGLE)
	{
		has_moved = true;
		old_reading = reading;
		return;
	}
	has_moved = false;
}

bool CENTER_PIECE::is_se()
{
	update();
	return (reading & (long)Center::SW_NE) == (long)Center::SE_NW;
}

 bool CENTER_PIECE::is_nw()
{
	update();
	return (reading & (long)Center::SE_NW) == (long)Center::SW_NE;
}

bool CENTER_PIECE::center_exists()
{
	update();
	return !new_center; //0 = true, 1 = false
}

//not implemented yet
int CENTER_PIECE::rotation_pos()
{
	return -1;
}

//not implemented yet
String CENTER_PIECE::center_alignment()
{
	return "";
}

//not implemented yet
String CENTER_PIECE::rotation_direction()
{
	return "";
}