#ifndef BOARD_VEMOS_MINI_D1_ESP8266_VERSION3_H
#define BOARD_VEMOS_MINI_D1_ESP8266_VERSION3_H

//esp8266 174 lights

//Positions in cross
#define SOUTH 0 //light furthest away from center
#define SOUTH_LOWER 0
#define SOUTH_UPPER 10

#define NORTH 21
#define NORTH_LOWER 11
#define NORTH_UPPER 21

#define EAST 22
#define EAST_LOWER 22
#define EAST_UPPER 32

#define WEST 43
#define WEST_LOWER 33
#define WEST_UPPER 43

//base starts closest to NORTH
#define NE_INNER 
#define NE_INNER_UPPER 81
#define NE_INNER_LOWER 63

#define NE_MIDDLE 
#define NE_MIDDLE_UPPER 141
#define NE_MIDDLE_LOWER 131

#define NE_OUTER 
#define NE_OUTER_UPPER 169
#define NE_OUTER_LOWER 167


#define NW_INNER 
#define NW_INNER_UPPER 100
#define NW_INNER_LOWER 82

#define NW_MIDDLE 
#define NW_MIDDLE_UPPER 152
#define NW_MIDDLE_LOWER 142

#define NW_OUTER 
#define NW_OUTER_UPPER 170
#define NW_OUTER_LOWER 172
//base starts from button closest to EAST

#define SE_INNER 
#define SE_INNER_UPPER 62
#define SE_INNER_LOWER 44

#define SE_MIDDLE 
#define SE_MIDDLE_UPPER 120
#define SE_MIDDLE_LOWER 130

#define SE_OUTER  //only 2 buttons
#define SE_OUTER_UPPER 166
#define SE_OUTER_LOWER 164

#define SW_INNER 
#define SW_INNER_UPPER 119
#define SW_INNER_LOWER 101

#define SW_MIDDLE 
#define SW_MIDDLE_UPPER 163
#define SW_MIDDLE_LOWER 153

#define SW_OUTER 
#define SW_OUTER_UPPER 175
#define SW_OUTER_LOWER 173

//size of each wing
#define CROSS_DIRECTION_SIZE 10
#define INNER_DIRECTION_SIZE 19
#define MIDDLE_DIRECTION_SIZE 11
#define OUTER_DIRECTION_SIZE 3

#define PAUSE_SCREEN_LIGHT_DELAY_MS 75
#define PAUSE_SCREEN_10_DELAY 15

#define MAX_SIZE 175

#define VIBRATOR_PIN 7
#define VIBRATOR_JOBLIST_SIZE 3

#define SMALL_VIBRATION 150
#define LARGE_VIBRATION 255

#endif // !BOARD_VEMOS_MINI_D1_ESP8266_H_
