#include "hardwarepins_12f_esp8266.h"

//SN74HC165N.
BYTES_VAL_T read_shift_regs()
{
	long bitVal;
	BYTES_VAL_T bytesVal = 0;

	/* Trigger a parallel Load to latch the state of the data lines,*/
	// digitalWrite(clockEnablePin, HIGH);
	digitalWrite(ploadPin, LOW);
	delayMicroseconds(PULSE_WIDTH_USEC);
	digitalWrite(ploadPin, HIGH);
	// digitalWrite(clockEnablePin, LOW);

	/* Loop to read each bit value from the serial out line of the SN74HC165N.*/
	for (int i = 0; i < DATA_WIDTH; i++)
	{
		bitVal = digitalRead(miso);

		/* Set the corresponding bit in bytesVal.*/
		bytesVal |= (bitVal << ((DATA_WIDTH - 1) - i));

		/* Pulse the Clock (rising edge shifts the next bit).*/
		digitalWrite(clockPin, HIGH);
		delayMicroseconds(PULSE_WIDTH_USEC);
		digitalWrite(clockPin, LOW);
	}

	return (bytesVal);
}