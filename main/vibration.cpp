#include "vibration_control.h"

unsigned int vibration_counter1 = 0, vibration_counter2 = 0, vibration_counter3 = 0, vibration_counter4 = 0;
unsigned int vibration_interval1 = 18, vibration_interval2 = 28, vibration_interval3 = 100, vibration_interval4 = 20;
bool vibration_toggl1 = LOW, vibration_toggl2 = LOW, vibration_toggl3 = LOW, vibration_toggl4 = LOW;
unsigned int actuator_value1, actuator_value2, actuator_value3, actuator_value4;


void vibrate(int type) 
{

	if (type == 1) {
		//Serial.println("type....1");
		vibration_toggl1 = HIGH;
		vibration_counter1 = 0;
	}
	if (type == 2) {
		//Serial.println("type....2");
		vibration_toggl2 = HIGH;
		vibration_counter2 = 0;
	}
	if (type == 3) {
		//Serial.println("type....3");
		vibration_toggl3 = HIGH;
		vibration_counter3 = 0;
	}
	if (type == 4) {
		//Serial.println("type....4");
		vibration_toggl4 = HIGH;
		vibration_counter4 = 0;
	}
}

void vibrate_update()
{
    if (vibration_toggl1) {
        if (vibration_counter1 <= vibration_interval1) {
            actuator_value1 = map(vibration_counter1, 0, vibration_interval1, 255, 110);
            analogWrite(7, actuator_value1);
            vibration_counter1++;
        }
        else { //reset
            vibration_toggl1 = LOW;
            analogWrite(7, 0);
            vibration_counter1 = 0;
        }
    }
    if (vibration_toggl2) {
        if (vibration_counter2 <= vibration_interval2) {

            if (vibration_counter2 <= vibration_interval2 / 2) {
                actuator_value2 = map(vibration_counter2, 0, vibration_interval2 / 2, 255, 20);
                analogWrite(7, actuator_value2);
            }
            else if (vibration_counter2 > vibration_interval2 / 2) {
                actuator_value4 = map(vibration_counter2, vibration_interval2 / 2, vibration_interval2, 20, 255);
                analogWrite(7, actuator_value4);
            }
            vibration_counter2++;
        }
        else {
            vibration_toggl2 = LOW;
            analogWrite(7, 0);
            vibration_counter2 = 0;
        }
    }
    if (vibration_toggl3) {
        if (vibration_counter3 <= vibration_interval3) {
            actuator_value3 = map(vibration_counter3, 0, vibration_interval3, 255, 100);
            analogWrite(7, actuator_value3);
            vibration_counter3++;
        }
        else {
            vibration_toggl3 = LOW;
            analogWrite(7, 0);
            vibration_counter3 = 0;
        }
    }
    if (vibration_toggl4) {
        if (vibration_counter4 <= vibration_interval4) {
            actuator_value4 = map(vibration_counter4, 0, vibration_interval4, 255, 160);
            analogWrite(7, actuator_value4);
            vibration_counter4++;
        }
        else {
            vibration_toggl4 = LOW;
            analogWrite(7, 0);
            vibration_counter4 = 0;
        }
    }
}