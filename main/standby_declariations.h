////deprecate this file
////arduino uno 182 lights
//
////Positions in cross
//#define SOUTH 0
//#define SOUTH_LOWER 0
//#define SOUTH_UPPER 11
//#define NORTH 23
//#define NORTH_LOWER 12
//#define NORTH_UPPER 23
//#define WEST 47
//#define WEST_LOWER 36
//#define WEST_UPPER 47
//#define EAST 24
//#define EAST_LOWER 24
//#define EAST_UPPER 35
////base starts closest to NORTH
//#define NE_INNER 85
//#define NE_INNER_UPPER 85
//#define NE_INNER_LOWER 67
//
//
//#define NE_MIDDLE 147
//#define NE_MIDDLE_UPPER 147
//#define NE_MIDDLE_LOWER 136
//
//
//#define NE_OUTER 176
//#define NE_OUTER_UPPER 176
//#define NE_OUTER_LOWER 174
//
//#define NW_INNER 86
//#define NW_INNER_UPPER 104
//#define NW_INNER_LOWER 86
//
//
//#define NW_MIDDLE 148
//#define NW_MIDDLE_UPPER 159
//#define NW_MIDDLE_LOWER 148
//
//#define NW_OUTER 177
//#define NW_OUTER_UPPER 179
//#define NW_OUTER_LOWER 177
////base starts from button closest to EAST
//
//#define SE_INNER 48
//#define SE_INNER_UPPER 66
//#define SE_INNER_LOWER 48
//
//#define SE_MIDDLE 124
//#define SE_MIDDLE_UPPER 135
//#define SE_MIDDLE_LOWER 124
//
//#define SE_OUTER 172 //only 2 buttons
//#define SE_OUTER_UPPER 173
//#define SE_OUTER_LOWER 172
//
//#define SW_INNER 123
//#define SW_INNER_UPPER 123
//#define SW_INNER_LOWER 105
//
//#define SW_MIDDLE 171
//#define SW_MIDDLE_UPPER 171
//#define SW_MIDDLE_LOWER 160
//
//#define SW_OUTER 182
//#define SW_OUTER_UPPER 182
//#define SW_OUTER_LOWER 180
//
////size of each wing
//#define CROSS_DIRECTION_SIZE 11
//#define INNER_DIRECTION_SIZE 19
//#define MIDDLE_DIRECTION_SIZE 12
//#define OUTER_DIRECTION_SIZE 3
//
//#define PAUSE_SCREEN_LIGHT_DELAY_MS 75
//#define PAUSE_SCREEN_10_DELAY 15
//
//#define MAX_SIZE 182
//
//#define VIBRATOR_PIN 7
//#define VIBRATOR_JOBLIST_SIZE 3
//
//#define SMALL_VIBRATION 150
//#define LARGE_VIBRATION 255