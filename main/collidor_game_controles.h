#ifndef _COLLIDOR_GAME_CONTROLS_H
#define _COLLIDOR_GAME_CONTROLS_H

// directions supported:
enum  direction { FORWARD, REVERSE };

// PARTICLES Class - derived from the FastLED class
class PARTICLES
{
  public:

    direction Direction1;      // direction to run the pattern
    direction Direction2;
    direction Direction3;
    direction Direction4;
    direction Direction5;

    unsigned long Interval1;   // milliseconds between updates
    unsigned long Interval2;
    unsigned long Interval3;
    unsigned long Interval4;
    unsigned long Interval5;

    uint16_t lane_start1;
    uint16_t lane_start2;
    uint16_t lane_start3;
    uint16_t lane_start4;
    uint16_t lane_start5;

    uint16_t lane_end1;
    uint16_t lane_end2;
    uint16_t lane_end3;
    uint16_t lane_end4;
    uint16_t lane_end5;

    uint16_t Index1 = random(48, 172);  // current step within the rotation (use to track the location of the particles)
    uint16_t Index2 = random(48, 172);
    uint16_t Index3 = random(48, 172);
    uint16_t Index4 = random(48, 172);
    uint16_t Index5 = random(48, 172);

    bool run_flag1;         // used to switch particles on/off
    bool run_flag2;
    bool run_flag3;
    bool run_flag4;
    bool run_flag5;

    void (*OnCkeckCollisionPoint1)(); //checks if particle is at collsion point
    void (*OnCkeckCollisionPoint2)();
    void (*OnCkeckCollisionPoint3)();
    void (*OnCkeckCollisionPoint4)();
    void (*OnCkeckCollisionPoint5)();

    PARTICLES( void (*checkPoint1)(), void (*checkPoint2)(), void (*checkPoint3)(), void (*checkPoint4)(), void (*checkPoint5)())
    {
      OnCkeckCollisionPoint1 = checkPoint1;
      OnCkeckCollisionPoint2 = checkPoint2;
      OnCkeckCollisionPoint3 = checkPoint3;
      OnCkeckCollisionPoint4 = checkPoint4;
      OnCkeckCollisionPoint5 = checkPoint5;
    }

    void particle_1_init (int interval, direction dir) {
      Interval1 = interval;
      Direction1 = dir;
    }
    void particle_2_init (int interval, direction dir) {
      Interval2 = interval;
      Direction2 = dir;
    }
    void particle_3_init (int interval, direction dir) {
      Interval3 = interval;
      Direction3 = dir;
    }
    void particle_4_init (int interval, direction dir) {
      Interval4 = interval;
      Direction4 = dir;
    }
    void particle_5_init (int interval, direction dir) {
      Interval5 = interval;
      Direction5 = dir;
    }

    int update_pos1( int lane[2] )
    {
      lane_start1 = lane[0];
      lane_end1 = lane[1];

      EVERY_N_MILLISECONDS(Interval1)
      {
        Increment1();
        OnCkeckCollisionPoint1();
      }
      return (Index1);
    }

    int update_pos2(int lane[2])
    {
      lane_start2 = lane[0];
      lane_end2 = lane[1];

      EVERY_N_MILLISECONDS(Interval2)
      {
        Increment2();
        OnCkeckCollisionPoint2();
      }
      return (Index2);
    }

    int update_pos3(int lane[2] )
    {
      lane_start3 = lane[0];
      lane_end3 = lane[1];

      EVERY_N_MILLISECONDS(Interval3)
      {
        Increment3();
        OnCkeckCollisionPoint3();
      }
      return (Index3);
    }

    int update_pos4(int lane[2] )
    {
      lane_start4 = lane[0];
      lane_end4 = lane[1];

      EVERY_N_MILLISECONDS(Interval4)
      {
        Increment4();
        OnCkeckCollisionPoint4();
      }
      return (Index4);
    }

    int update_pos5(int lane[2] )
    {
      lane_start5 = lane[0];
      lane_end5 = lane[1];

      EVERY_N_MILLISECONDS(Interval5)
      {
        Increment5();
        OnCkeckCollisionPoint5();
      }
      return (Index5);
    }


    // Increment the Index and reset at the end
    void Increment1()
    {
      if (Direction1 == FORWARD)
      {
        Index1++;
        if (Index1 >= lane_end1)
        {
          Index1 = lane_start1;
        }
      }
      else // Direction == REVERSE
      {
        if ( Index1 <= lane_start1 )
        {
          Index1 = lane_end1;
        }
        --Index1;
      }
    }

    void Increment2()
    {
      if (Direction2 == FORWARD)
      {
        Index2++;
        if (Index2 >= lane_end2)
        {
          Index2 = lane_start2;
        }
      }
      else // Direction == REVERSE
      {
        if ( Index2 <= lane_start2 )
        {
          Index2 = lane_end2;
        }
        --Index2;
      }
    }

    void Increment3()
    {
      if (Direction3 == FORWARD)
      {
        Index3++;
        if (Index3 >= lane_end3)
        {
          Index3 = lane_start3;
        }
      }
      else // Direction == REVERSE
      {
        if (Index3 <= lane_start3)
        {
          Index3 = lane_end3 ;
        }
        --Index3;
      }
    }

    void Increment4()
    {
      if (Direction4 == FORWARD)
      {
        Index4++;
        if (Index4 >= lane_end4)
        {
          Index4 = lane_start4;
        }
      }
      else // Direction == REVERSE
      {
        if (Index4 <= lane_start4)
        {
          Index4 = lane_end4 - 1;
        }
        --Index4;
      }
    }

    void Increment5()
    {
      if (Direction5 == FORWARD)
      {
        Index5++;
        if (Index5 >= lane_end5)
        {
          Index5 = lane_start5;
        }
      }
      else
      {
        if (Index5 <= lane_start5)
        {
          Index5 = lane_end5 - 1;
        }
        --Index5;
      }
    }

    // Reverse pattern direction
    void Reverse1(byte offset)
    {
      if ( Direction1 == FORWARD )
      {
        Direction1 = REVERSE;
        Index1 = Index1 + offset;
      }
      else
      {
        Direction1 = FORWARD;
        Index1 = Index1 - offset;
      }
    }

    void Reverse2(byte offset)
    {
      if (Direction2 == FORWARD)
      {
        Direction2 = REVERSE;
        Index2 = Index2 + offset;
      }
      else
      {
        Direction2 = FORWARD;
        Index2 = Index2 - offset;
      }
    }

    void Reverse3(byte offset)
    {
      if (Direction3 == FORWARD)
      {
        Direction3 = REVERSE;
        Index3 = Index3 + offset;
      }
      else
      {
        Direction3 = FORWARD;
        Index3 = Index3 - offset;
      }
    }

    // Reverse pattern direction
    void Reverse4(byte offset)
    {
      if (Direction4 == FORWARD)
      {
        Direction4 = REVERSE;
        Index4 = Index4 + offset;
      }
      else
      {
        Direction4 = FORWARD;
        Index4 = Index4 - offset;
      }
    }

    // Reverse pattern direction
    void Reverse5(byte offset)
    {
      if (Direction5 == FORWARD)
      {
        Direction5 = REVERSE;
        Index5 = Index5 + offset;
      }
      else
      {
        Direction5 = FORWARD;
        Index5 = Index5 - offset;
      }
    }
};


void particle1_check_points();
void particle2_check_points();
void particle3_check_points();
void particle4_check_points();
void particle5_check_points();

PARTICLES particle( &particle1_check_points, &particle2_check_points, &particle3_check_points, &particle4_check_points, &particle5_check_points );

uint16_t pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0, pos5 = 0;

int TOP_BOTTOM[] = {0, 24};
int RIGHT_LEFT[] = {24, 48};
int INNER_RING[] = {48, 124};
int MID_RING[]   = {124, 172};
int OUTER_RING[] = {172, 185};

int TOP_BOTTOM_CHECKPOINTS[] = {0, 12, 23};
int RIGHT_LEFT_CHECKPOINTS[] = {24, 36, 47};
int INNER_RING_CHECKPOINTS[] = {48, 66, 67, 85, 86, 104, 105, 123};
int MID_RING_CHECKPOINTS[]   = {124, 135, 136, 147, 148, 159, 160, 171};
int OUTER_RING_CHECKPOINTS[] = {172, 173, 174, 176, 177, 179, 180, 182};

int particle1_on_lane[2], particle2_on_lane[2], particle3_on_lane[2], particle4_on_lane[2], particle5_on_lane[2];

bool input_colors_flag1 = LOW;
bool input_colors_flag2 = LOW;
bool input_colors_flag3 = LOW;

bool explosion_flag = LOW;

CRGB target_colors[4];

CRGB particle_colors[] = { 0xF03316, 0xE8F124, 0x24F15F, 0xEE24F1 };

CRGB particle1_color = particle_colors[0];
CRGB particle2_color = particle_colors[1];
CRGB particle3_color = particle_colors[2];
CRGB particle4_color = particle_colors[3];

CRGB input_colors[3];
CRGB newColor;

byte random_target_position;
byte active_particles = 0;

void reset_center_colors() {

  byte random_target_position_holder = random(1, 5); //between 1 - 4
  Serial.println(random_target_position_holder);

  if ( random_target_position == random_target_position_holder ) {

    Serial.print("rolling the dice again...");

    if (random_target_position_holder == 4) {
      random_target_position = random_target_position_holder - 1;
    }
    else {
      random_target_position = random_target_position_holder + 1;
    }
    Serial.println(random_target_position);
  }
  else {
    random_target_position = random_target_position_holder;
  }

  if ( active_particles == 1 ) {
    target_colors[0] = particle_colors[0];
  }
  else if ( active_particles == 2 ) {
    target_colors[0] = particle_colors[0];
    target_colors[1] = particle_colors[1];
  }
  else if ( active_particles == 3 ) {
    target_colors[0] = particle_colors[0];
    target_colors[1] = particle_colors[1];
    target_colors[2] = particle_colors[2];
  }
  else if ( active_particles == 4 ) {
    target_colors[0] = particle_colors[0];
    target_colors[1] = particle_colors[1];
    target_colors[2] = particle_colors[2];
    target_colors[3] = particle_colors[3];
  }
}

void spawn_particle() {

  copy(INNER_RING, particle1_on_lane);
  copy(INNER_RING, particle2_on_lane);

  particle.Index1 = random(48, 172);  // current step within the rotation (use to track the location of the particles)
  particle.Index2 = random(48, 172);

  reset_center_colors();

  FastLED.show();
  explosion_flag = HIGH;
}

void explosion( CRGB particle_color, byte particle_num, bool toggle ) {

  Serial.print("particle_num: "); Serial.println(particle_num);

  if ( particle_num == 1 ) {
    input_colors[0] = particle_color;
    input_colors_flag1 = HIGH;
  }
  if ( particle_num == 2 ) {
    input_colors[1] = particle_color;
    input_colors_flag2 = HIGH;
  }

  if ( input_colors_flag1 && input_colors_flag2 ) {

    // Use pixelColor to define color of new particles
    newColor = blend(input_colors[0], input_colors[1], 125); //125 blends colors equally ( 255/2 )

    CRGB gradient1 = blend(input_colors[0], input_colors[1], 100);
    CRGB gradient2 = blend(input_colors[0], input_colors[1], 50);
    CRGB gradient3 = blend(input_colors[0], input_colors[1], 10);

    for ( int i = 0; i < NUM_LEDS; i++) {
      if ( i <= RIGHT_LEFT[1] ) {
        leds[i] = newColor;
      }
      if (i >= RIGHT_LEFT[1] && i <= INNER_RING[1]) {
        leds[i] = gradient1;
      }
      if (i >= INNER_RING[1] && i <= MID_RING[1]) {
        leds[i] = gradient2;
      }
      if ( i >= MID_RING[1] && i <= OUTER_RING[1]) {
        leds[i] = gradient3;
      }
    }

    input_colors_flag1 = LOW;
    input_colors_flag2 = LOW;

    FastLED.show();
    //delay(1000);
    spawn_particle();
  }
}

void particle1_check_points() {

  // ------------- TOP <--> BOTTOM ------------- //
  if (particle1_on_lane[0] == TOP_BOTTOM[0])
  {
    // ----------------- 1 ----------------- //
    if (pos1 == TOP_BOTTOM_CHECKPOINTS[0] && particle.Direction1 == REVERSE )
    {
      if (button_states.substring(27, 28) == "1" && button_states.substring(26, 27) == "1" )
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[7];
        vibrate(2);
      }
      else if (button_states.substring(27, 28) == "1" && button_states.substring(28, 29) == "1" )
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(27, 28) == "1" )
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ----------------- 2 ----------------- //
    else if ((pos1 >= 10) && (pos1 <= 13))
    {
      center_in_place = center.center_in_place();
      center_aligemnt = center.center_alignment();

      // center is off //
      if ( center_in_place )
      {
        if ( particle.Direction1 == REVERSE )
        {
          if (particle1_color == target_colors[0])
          {
            explosion( particle1_color, 1, HIGH );
          }
        }
        else if  (particle.Direction1 == FORWARD )
        {
          if (particle1_color == target_colors[1])
          {
            explosion( particle1_color, 1, HIGH );
          }
        }
      }

      // center is on //
      else if (!center_in_place)
      {
        if ( center_aligemnt == "right" ) {

          if (particle.Direction1 == FORWARD) {
            particle.Index1 = 9;
            particle.Reverse1(0);
          }
          else if ( particle.Direction1 == REVERSE) {
            particle.Index1 = 14;
            particle.Reverse1(0);
          }
        }
        else  {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos1 == 9 or pos1 == 14 ) {
      input_colors_flag1 = LOW;
    }

    // ----------------- 3 ----------------- //
    else if ( pos1 == TOP_BOTTOM_CHECKPOINTS[2] && particle.Direction1 == FORWARD )
    {
      if (button_states.substring(11, 12) == "1" && button_states.substring(10, 11) == "1" )
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(11, 12) == "1" && button_states.substring(12, 13) == "1" )
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[4];
        vibrate(2);
      }
      else if (button_states.substring(11, 12) == "1" )
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }
  }

  // ------------- RIGHT <--> LEFT ------------- //
  if ( particle1_on_lane[0] == RIGHT_LEFT[0] )
  {
    // ------------- 0 ------------- //
    if (pos1 == RIGHT_LEFT_CHECKPOINTS[0] && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(19, 20) == "1" && button_states.substring(20, 21) == "1")
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(19, 20) == "1" && button_states.substring(18, 19) == "1")
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[1];
        vibrate(2);
      }
      else if (button_states.substring(19, 20) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ------------- 1 ------------- //
    else if ((pos1 >= 33) && (pos1 <= 37))
    {
      center_in_place = center.center_in_place();
      center_aligemnt = center.center_alignment();
      Serial.println(center_aligemnt);

      /* center is off */
      if (center_in_place)
      {
        if (particle.Direction1 == FORWARD)
        {
          if (particle1_color == target_colors[3])
          {
            explosion(particle1_color, 1, HIGH);
          }
        }
        else if (particle.Direction1 == REVERSE)
        {
          if (particle1_color == target_colors[2])
          {
            explosion(particle1_color, 1, HIGH);
          }
        }
      }

      /* center is on */
      else if (!center_in_place)
      {
        if ( center_aligemnt == "left" or center_aligemnt == "null" ) { //only temp!!!
          if ( particle.Direction1 == FORWARD ) {
            particle.Index1 = 32;
            particle.Reverse1(0);
          }
          else if ( particle.Direction1 == REVERSE) {
            particle.Index1 = 38;
            particle.Reverse1(0);
          }
        }
        else {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos1 == 32 or pos1 == 38 ) {
      input_colors_flag1 = LOW;
    }

    // ------------- 2 ------------- //
    else if (pos1 == RIGHT_LEFT_CHECKPOINTS[2] && particle.Direction1 == FORWARD)
    {
      if (button_states.substring(3, 4) == "1" && button_states.substring(2, 3) == "1" )
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(3, 4) == "1" && button_states.substring(4, 5) == "1" )
      {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[6];
        vibrate(2);
      }
      else if (button_states.substring(3, 4) == "1" )
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }
  }

  // ------------- INNER <--> RING ------------- //
  else if (particle1_on_lane[0] == INNER_RING[0])
  {

    // ----------------- 0 ----------------- //
    if ((pos1 == INNER_RING_CHECKPOINTS[0] + 1) && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(28, 29) == "1" && button_states.substring(27, 28) == "1")
      {
        copy(TOP_BOTTOM, particle1_on_lane);
        particle.Index1 = TOP_BOTTOM_CHECKPOINTS[0];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(28, 29) == "1" && button_states.substring(29, 30) == "1")
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[0];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(28, 29) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }
    // ----------------- 1 ----------------- ///
    else if ((pos1 == INNER_RING_CHECKPOINTS[1] - 1) && particle.Direction1 == FORWARD)
    {
      if (button_states.substring(18, 19) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle1_on_lane);
        particle.Index1 = RIGHT_LEFT_CHECKPOINTS[0];
        vibrate(2);
      }
      else if (button_states.substring(18, 19) == "1" && button_states.substring(17, 18) == "1")
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[1];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(18, 19) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ----------------- 2 ----------------- ///
    else if ((pos1 == INNER_RING_CHECKPOINTS[2] + 1) && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(20, 21) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle1_on_lane);
        particle.Index1 = RIGHT_LEFT_CHECKPOINTS[0];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(20, 21) == "1" && button_states.substring(21, 22) == "1")
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[2];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(20, 21) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ----------------- 3 ----------------- //
    else if (( pos1 == INNER_RING_CHECKPOINTS[3] - 1 ) && particle.Direction1 == FORWARD)
    {
      if (button_states.substring(10, 11) == "1" && button_states.substring(11, 12) == "1")
      {
        copy(TOP_BOTTOM, particle1_on_lane);
        particle.Index1 = TOP_BOTTOM_CHECKPOINTS[2];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(10, 11) == "1" && button_states.substring(9, 10) == "1" )
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[3];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(10, 11) == "1" )
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ----------------- 4 ----------------- //
    else if (( pos1 == INNER_RING_CHECKPOINTS[4]  + 1) && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(12, 13) == "1" && button_states.substring(11, 12) == "1" )
      {
        copy(TOP_BOTTOM, particle1_on_lane);
        particle.Index1 = TOP_BOTTOM_CHECKPOINTS[2];
        vibrate(2);
      }
      else if (button_states.substring(12, 13) == "1" && button_states.substring(13, 14) == "1" )
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[4];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(12, 13) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ----------------- 5 ----------------- //
    else if (( pos1 == INNER_RING_CHECKPOINTS[5] - 1 ) && particle.Direction1 == FORWARD)
    {
      if (button_states.substring(2, 3) == "1" && button_states.substring(1, 2) == "1" )
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[5];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(2, 3) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle1_on_lane);
        particle.Index1 = RIGHT_LEFT_CHECKPOINTS[2];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(2, 3) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ----------------- 6 ----------------- //
    else if ((pos1 == INNER_RING_CHECKPOINTS[6]  + 1 ) && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(4, 5) == "1" && button_states.substring(5, 6) == "1" )
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[6];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(4, 5) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle1_on_lane);
        particle.Index1 = RIGHT_LEFT_CHECKPOINTS[2];
        vibrate(2);
      }
      else if (button_states.substring(4, 5) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ----------------- 7 ----------------- //
    else if (( pos1 == INNER_RING_CHECKPOINTS[7] - 1 ) && particle.Direction1 == FORWARD)
    {
      if (button_states.substring(26, 27) == "1" && button_states.substring(25, 26) == "1" )
      {
        copy(MID_RING, particle1_on_lane);
        particle.Index1 = MID_RING_CHECKPOINTS[7];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(26, 27) == "1" && button_states.substring(27, 28) == "1" )
      {
        copy(TOP_BOTTOM, particle1_on_lane);
        particle.Index1 = TOP_BOTTOM_CHECKPOINTS[0];
        vibrate(2);
      }
      else if (button_states.substring(26, 27) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }
  }

  // ---------------------- MID <--> RING ---------------------- //
  else if ( particle1_on_lane[0] == MID_RING[0] )
  {
    if (( pos1 == MID_RING_CHECKPOINTS[0] + 1 ) && particle.Direction1 == REVERSE)
    {
      // ------------------- 0 ------------------------//
      if (button_states.substring(29, 30) == "1" && button_states.substring(28, 29) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(29, 30) == "1" && button_states.substring(30, 31) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[0];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(29, 30) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    else if (( pos1 == MID_RING_CHECKPOINTS[1] - 1 ) && particle.Direction1 == FORWARD)
    {
      // ------------------- 1 ------------------------//
      if (button_states.substring(17, 18) == "1" && button_states.substring(18, 19) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[1];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(17, 18) == "1" && button_states.substring(16, 17) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[1];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(17, 18) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ------------------- 2 ------------------------//
    else if (( pos1 == MID_RING_CHECKPOINTS[2] + 1 ) && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(21, 22) == "1" && button_states.substring(20, 21) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(21, 22) == "1" && button_states.substring(22, 23) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[2];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(21, 22) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ------------------- 3 ------------------------//
    else if (( pos1 == MID_RING_CHECKPOINTS[3]  - 1 ) && particle.Direction1 == FORWARD)
    {
      if (button_states.substring(9, 10) == "1" && button_states.substring(10, 11) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(9, 10) == "1" && button_states.substring(8, 9) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[3];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(9, 10) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ------------------- 4 ------------------------//
    else if (pos1 == MID_RING_CHECKPOINTS[4] + 1 && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(13, 14) == "1" && button_states.substring(12, 13) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[4];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(13, 14) == "1" && button_states.substring(14, 15) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[4];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(13, 14) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ------------------- 5 ------------------------//
    else if (( pos1 == MID_RING_CHECKPOINTS[5] - 1 ) && particle.Direction1 == FORWARD)
    {
      if (button_states.substring(1, 2) == "1" && button_states.substring(2, 3) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(1, 2) == "1" && button_states.substring(0, 1) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[5];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(1, 2) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ------------------- 6 ------------------------//
    else if (( pos1 == MID_RING_CHECKPOINTS[6] - 1 ) && particle.Direction1 == REVERSE)
    {
      if (button_states.substring(5, 6) == "1" && button_states.substring(4, 5) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[6];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(5, 6) == "1" && button_states.substring(6, 7) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[6];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(5, 6) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }

    // ------------------- 7 ------------------------//
    else if (( pos1 == MID_RING_CHECKPOINTS[7] + 1 ) && particle.Direction1 == FORWARD )
    {
      if (button_states.substring(25, 26) == "1" && button_states.substring(26, 27) == "1") {
        copy(INNER_RING, particle1_on_lane);
        particle.Index1 = INNER_RING_CHECKPOINTS[7];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(25, 26) == "1" && button_states.substring(24, 25) == "1") {
        copy(OUTER_RING, particle1_on_lane);
        particle.Index1 = OUTER_RING_CHECKPOINTS[7];
        particle.Reverse1(0);
        vibrate(2);
      }
      else if (button_states.substring(25, 26) == "1")
      {
        particle.Reverse1(0);
        vibrate(1);
      }
    }
  }
  else
  {
    //Serial.println("particle1 does not exist inside the lane constrains...");
  }
}
void particle2_check_points() {

  // ------------- TOP <--> BOTTOM ------------- //
  if (particle2_on_lane[0] == TOP_BOTTOM[0])
  {
    // ----------------- 1 ----------------- //
    if (pos2 == TOP_BOTTOM_CHECKPOINTS[0] && particle.Direction2 == REVERSE)
    {
      if (button_states.substring(27, 28) == "1" && button_states.substring(26, 27) == "1" )
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[7];
        vibrate(2);
      }
      else if (button_states.substring(27, 28) == "1" && button_states.substring(28, 29) == "1" )
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(27, 28) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ----------------- 2 ----------------- //
    else if ((pos2 >= 10) && (pos2 <= 13))
    {
      center_in_place = center.center_in_place();
      center_aligemnt = center.center_alignment();

      // center is off //
      if (center_in_place)
      {
        if (particle.Direction2 == REVERSE)
        {
          if (particle2_color == target_colors[0])
          {
            explosion(particle2_color, 2, HIGH);
          }
        }
        else if (particle.Direction2 == FORWARD)
        {
          if (particle2_color == target_colors[1])
          {
            explosion(particle2_color, 2, HIGH);
          }
        }
      }

      // center is on //
      else if (!center_in_place)
      {
        if ( center_aligemnt == "right" ) {

          if (particle.Direction2 == FORWARD) {
            particle.Index2 = 9;
            particle.Reverse2(0);
          }
          else if ( particle.Direction2 == REVERSE) {
            particle.Index2 = 14;
            particle.Reverse2(0);
          }
        }
        else  {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos2 == 9 or pos2 == 14 ) {
      input_colors_flag2 = LOW;
    }

    // ----------------- 3 ----------------- //
    else if (pos2 == TOP_BOTTOM_CHECKPOINTS[2] && particle.Direction2 == FORWARD)
    {
      if (button_states.substring(11, 12) == "1" && button_states.substring(10, 11) == "1" )
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(11, 12) == "1" && button_states.substring(12, 13) == "1")
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[4];
        vibrate(2);
      }
      else if (button_states.substring(11, 12) == "1" )
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }
  }

  // ------------- RIGHT <--> LEFT ------------- //
  if (particle2_on_lane[0] == RIGHT_LEFT[0])
  {
    // ------------- 0 ------------- //
    if (pos2 == RIGHT_LEFT_CHECKPOINTS[0] && particle.Direction2 == REVERSE )
    {
      if (button_states.substring(19, 20) == "1" && button_states.substring(20, 21) == "1" )
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(19, 20) == "1" && button_states.substring(18, 19) == "1" )
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[1];
        vibrate(2);
      }
      else if (button_states.substring(19, 20) == "1" )
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ------------- 1 ------------- //
    else if ((pos2 >= 33) && (pos2 <= 37))
    {
      center_in_place = center.center_in_place();

      if (center_in_place)
      {
        if (particle.Direction2 == FORWARD)
        {
          if (particle2_color == target_colors[3])
          {
            explosion(particle2_color, 2, HIGH);
          }
        }
        else if (particle.Direction2 == REVERSE)
        {
          if (particle2_color == target_colors[2])
          {
            explosion(particle2_color, 2, HIGH);
          }
        }
      }

      /* center is on */
      else if (!center_in_place)
      {
        if ( center_aligemnt == "left" or center_aligemnt == "null" ) { //only temp!!!
          if ( particle.Direction2 == FORWARD ) {
            particle.Index2 = 32;
            particle.Reverse2(0);
          }
          else if ( particle.Direction2 == REVERSE) {
            particle.Index2 = 38;
            particle.Reverse2(0);
          }
        }
        else {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos2 == 32 or pos2 == 38 ) {
      input_colors_flag2 = LOW;
    }
    // ------------- 2 ------------- //
    else if (pos2 == RIGHT_LEFT_CHECKPOINTS[2] && particle.Direction2 == FORWARD )
    {
      if (button_states.substring(3, 4) == "1" && button_states.substring(2, 3) == "1" )
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(3, 4) == "1" && button_states.substring(4, 5) == "1" )
      {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[6];
        vibrate(2);
      }
      else if (button_states.substring(3, 4) == "1" )
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }
  }

  // ------------- INNER <--> RING ------------- //
  else if (particle2_on_lane[0] == INNER_RING[0])
  {
    // ----------------- 0 ----------------- //
    if (( pos2 == INNER_RING_CHECKPOINTS[0]  + 1 ) && particle.Direction2 == REVERSE)
    {
      if (button_states.substring(28, 29) == "1" && button_states.substring(27, 28) == "1")
      {
        copy(TOP_BOTTOM, particle2_on_lane);
        particle.Index2 = TOP_BOTTOM_CHECKPOINTS[0];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(28, 29) == "1" && button_states.substring(29, 30) == "1")
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[0];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(28, 29) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }
    // ----------------- 1 ----------------- //
    else if (( pos2 == INNER_RING_CHECKPOINTS[1] - 1 ) && particle.Direction2 == FORWARD)
    {
      if (button_states.substring(18, 19) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle2_on_lane);
        particle.Index2 = RIGHT_LEFT_CHECKPOINTS[0];
        vibrate(2);
      }
      else if (button_states.substring(18, 19) == "1" && button_states.substring(17, 18) == "1")
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[1];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(18, 19) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ----------------- 2 ----------------- //
    else if (( pos2 == INNER_RING_CHECKPOINTS[2] + 1 ) && particle.Direction2 == REVERSE)
    {
      if (button_states.substring(20, 21) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle2_on_lane);
        particle.Index2 = RIGHT_LEFT_CHECKPOINTS[0];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(20, 21) == "1" && button_states.substring(21, 22) == "1")
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[2];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(20, 21) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ----------------- 3 ----------------- //
    else if (( pos2 == INNER_RING_CHECKPOINTS[3] - 1 ) && particle.Direction2 == FORWARD)
    {
      if (button_states.substring(10, 11) == "1" && button_states.substring(11, 12) == "1")
      {
        copy( TOP_BOTTOM, particle2_on_lane );
        particle.Index2 = TOP_BOTTOM_CHECKPOINTS[2];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(10, 11) == "1" && button_states.substring(9, 10) == "1" )
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[3];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(10, 11) == "1" )
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ----------------- 4 ----------------- //
    else if (( pos2 == INNER_RING_CHECKPOINTS[4] + 1 ) && particle.Direction2 == REVERSE)
    {
      if (button_states.substring(12, 13) == "1" && button_states.substring(11, 12) == "1" )
      {
        copy(TOP_BOTTOM, particle2_on_lane);
        particle.Index2 = TOP_BOTTOM_CHECKPOINTS[2];
        vibrate(2);
      }
      else if (button_states.substring(12, 13) == "1" && button_states.substring(13, 14) == "1" )
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[4];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(12, 13) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ----------------- 5 ----------------- //
    else if (( pos2 == INNER_RING_CHECKPOINTS[5] - 1 ) && particle.Direction2 == FORWARD)
    {
      if (button_states.substring(2, 3) == "1" && button_states.substring(1, 2) == "1" )
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[5];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(2, 3) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle2_on_lane);
        particle.Index2 = RIGHT_LEFT_CHECKPOINTS[2];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(2, 3) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ----------------- 6 ----------------- //
    else if (( pos2 == INNER_RING_CHECKPOINTS[6] + 1 ) && particle.Direction2 == REVERSE)
    {
      if (button_states.substring(4, 5) == "1" && button_states.substring(5, 6) == "1" )
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[6];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(4, 5) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle2_on_lane);
        particle.Index2 = RIGHT_LEFT_CHECKPOINTS[2];
        vibrate(2);
      }
      else if (button_states.substring(4, 5) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ----------------- 7 ----------------- //
    else if (( pos2 == INNER_RING_CHECKPOINTS[7] - 1 ) && particle.Direction2 == FORWARD)
    {
      if (button_states.substring(26, 27) == "1" && button_states.substring(25, 26) == "1" )
      {
        copy(MID_RING, particle2_on_lane);
        particle.Index2 = MID_RING_CHECKPOINTS[7];
        particle.Reverse2(0);
        vibrate(2);
      }
      else if (button_states.substring(26, 27) == "1" && button_states.substring(27, 28) == "1" )
      {
        copy(TOP_BOTTOM, particle2_on_lane);
        particle.Index2 = TOP_BOTTOM_CHECKPOINTS[0];
        vibrate(2);
      }
      else if (button_states.substring(26, 27) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }
  }

  // ---------------------- MID <--> RING ---------------------- //
  else if (particle2_on_lane[0] == MID_RING[0])
  {
    if (( pos2 == MID_RING_CHECKPOINTS[0] + 1 ) && particle.Direction2 == REVERSE)
    {
      // 0 //
      if (button_states.substring(29, 30) == "1" && button_states.substring(28, 29) == "1") {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse2(0);
      }
      else if (button_states.substring(29, 30) == "1" && button_states.substring(30, 31) == "1") {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[0];
        particle.Reverse2(0);
      }
      else if (button_states.substring(29, 30) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    else if (( pos2 == MID_RING_CHECKPOINTS[1] - 1 ) && particle.Direction2 == FORWARD)
    {
      // 1 //
      if (button_states.substring(17, 18) == "1" && button_states.substring(18, 19) == "1") {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[1];
        particle.Reverse2(0);
      }
      else if (button_states.substring(17, 18) == "1" && button_states.substring(16, 17) == "1") {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[1];
        particle.Reverse2(0);
      }
      else if (button_states.substring(17, 18) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    else if (( pos2 == MID_RING_CHECKPOINTS[2] + 1 ) && particle.Direction2 == REVERSE)
    {
      // 2 //
      if (button_states.substring(21, 22) == "1" && button_states.substring(20, 21) == "1") {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse2(0);
      }
      else if (button_states.substring(21, 22) == "1" && button_states.substring(22, 23) == "1") {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[2];
        particle.Reverse2(0);
      }
      else if (button_states.substring(21, 22) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // ------------------- 3 ------------------------//
    else if (( pos2 == MID_RING_CHECKPOINTS[3]  - 1 ) && particle.Direction2 == FORWARD)
    {
      if ( button_states.substring(9, 10) == "1" && button_states.substring(10, 11) == "1" ) {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse2(0);
      }
      else if ( button_states.substring(9, 10) == "1" && button_states.substring(8, 9) == "1" ) {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[3];
        particle.Reverse2(0);
      }
      else if (button_states.substring(9, 10) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // 4 //
    else if (pos2 == MID_RING_CHECKPOINTS[4] + 1 && particle.Direction2 == REVERSE)
    {
      if (button_states.substring(13, 14) == "1" && button_states.substring(12, 13) == "1") {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[4];
        particle.Reverse2(0);
      }
      else if (button_states.substring(13, 14) == "1" && button_states.substring(14, 15) == "1") {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[4];
        particle.Reverse2(0);
      }
      else if (button_states.substring(13, 14) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // 5 //
    else if (( pos2 == MID_RING_CHECKPOINTS[5] - 1 ) && particle.Direction2 == FORWARD)
    {
      if (button_states.substring(1, 2) == "1" && button_states.substring(2, 3) == "1") {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse2(0);
      }
      else if (button_states.substring(1, 2) == "1" && button_states.substring(0, 1) == "1") {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[5];
        particle.Reverse2(0);
      }
      else if (button_states.substring(1, 2) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // 6 //
    else if (( pos2 == MID_RING_CHECKPOINTS[6] - 1 ) && particle.Direction2 == REVERSE)
    {
      if (button_states.substring(5, 6) == "1" && button_states.substring(4, 5) == "1") {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[6];
        particle.Reverse2(0);
      }
      else if (button_states.substring(5, 6) == "1" && button_states.substring(6, 7) == "1") {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[6];
        particle.Reverse2(0);
      }
      else if (button_states.substring(5, 6) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }

    // 7 //
    else if (( pos2 == MID_RING_CHECKPOINTS[7] + 1 ) && particle.Direction2 == FORWARD )
    {
      if (button_states.substring(25, 26) == "1" && button_states.substring(26, 27) == "1") {
        copy(INNER_RING, particle2_on_lane);
        particle.Index2 = INNER_RING_CHECKPOINTS[7];
        particle.Reverse2(0);
      }
      else if (button_states.substring(25, 26) == "1" && button_states.substring(24, 25) == "1") {
        copy(OUTER_RING, particle2_on_lane);
        particle.Index2 = OUTER_RING_CHECKPOINTS[7];
        particle.Reverse2(0);
      }
      else if (button_states.substring(25, 26) == "1")
      {
        particle.Reverse2(0);
        vibrate(1);
      }
    }
  }
  else
  {
    // Serial.println("particle2 does not exist inside the lane constrains...");
  }
}
void particle3_check_points() {

  // ------------- TOP <--> BOTTOM ------------- //
  if (particle3_on_lane[0] == TOP_BOTTOM[0])
  {
    // ----------------- 1 ----------------- //
    if (pos3 == TOP_BOTTOM_CHECKPOINTS[0] && particle.Direction3 == REVERSE )
    {
      if (button_states.substring(27, 28) == "1" && button_states.substring(26, 27) == "1" )
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[7];
      }
      else if (button_states.substring(27, 28) == "1" && button_states.substring(28, 29) == "1" )
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse3(0);
      }
      else if (button_states.substring(27, 28) == "1" )
      {
        particle.Reverse3(0);
      }
    }

    // ----------------- 2 ----------------- //
    else if ( (pos3 >= 10) && (pos3 <= 13) )
    {
      center_in_place = center.center_in_place();
      center_aligemnt = center.center_alignment();
      Serial.println(center_aligemnt);

      // center is off //
      if ( center_in_place )
      {
        if ( particle.Direction3 == REVERSE )
        {
          if (particle1_color == target_colors[0])
          {
            explosion( particle1_color, 3, HIGH );
          }
        }
        else if  (particle.Direction3 == FORWARD )
        {
          if (particle1_color == target_colors[1])
          {
            explosion( particle1_color, 3, HIGH );
          }
        }
      }
      // center is on //
      else if (!center_in_place)
      {
        if ( center_aligemnt == "right" ) {
          if ( particle.Direction3 == FORWARD ) {
            particle.Index3 = 9;
            particle.Reverse3(0);
          }
          else if ( particle.Direction3 == REVERSE) {
            particle.Index3 = 14;
            particle.Reverse3(0);
          }
        }
        else  {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos3 == 9 or pos3 == 14 ) {
      input_colors_flag3 = LOW;
    }

    // ----------------- 3 ----------------- //
    else if ( pos3 == TOP_BOTTOM_CHECKPOINTS[2] && particle.Direction3 == FORWARD )
    {
      if (button_states.substring(11, 12) == "1" && button_states.substring(10, 11) == "1" )
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse3(0);
      }
      else if (button_states.substring(11, 12) == "1" && button_states.substring(12, 13) == "1" )
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[4];
      }
      else if (button_states.substring(11, 12) == "1" )
      {
        particle.Reverse3(0);
      }
    }
  }

  // ------------- RIGHT <--> LEFT ------------- //
  if ( particle3_on_lane[0] == RIGHT_LEFT[0] )
  {
    // ------------- 0 ------------- //
    if (pos3 == RIGHT_LEFT_CHECKPOINTS[0] && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(19, 20) == "1" && button_states.substring(20, 21) == "1")
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse3(0);
      }
      else if (button_states.substring(19, 20) == "1" && button_states.substring(18, 19) == "1")
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[1];
      }
      else if (button_states.substring(19, 20) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ------------- 1 ------------- //
    else if ((pos3 >= 33) && (pos3 <= 37))
    {
      center_in_place = center.center_in_place();
      center_aligemnt = center.center_alignment();
      Serial.println(center_aligemnt);

      /* center is off */
      if (center_in_place)
      {
        if (particle.Direction3 == FORWARD)
        {
          if (particle1_color == target_colors[3])
          {
            explosion(particle1_color, 3, HIGH);
          }
        }
        else if (particle.Direction3 == REVERSE)
        {
          if (particle1_color == target_colors[2])
          {
            explosion(particle1_color, 3, HIGH);
          }
        }
      }

      /* center is on */
      else if (!center_in_place)
      {
        if ( center_aligemnt == "left" or center_aligemnt == "null" ) { //only temp!!!
          if ( particle.Direction3 == FORWARD ) {
            particle.Index3 = 32;
            particle.Reverse3(0);
          }
          else if ( particle.Direction3 == REVERSE) {
            particle.Index3 = 38;
            particle.Reverse3(0);
          }
        }
        else {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos3 == 32 or pos3 == 38 ) {
      input_colors_flag3 = LOW;
    }

    // ------------- 2 ------------- //
    else if (pos3 == RIGHT_LEFT_CHECKPOINTS[2] && particle.Direction3 == FORWARD)
    {
      if (button_states.substring(3, 4) == "1" && button_states.substring(2, 3) == "1" )
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse3(0);
      }
      else if (button_states.substring(3, 4) == "1" && button_states.substring(4, 5) == "1" )
      {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[6];
      }
      else if (button_states.substring(3, 4) == "1" )
      {
        particle.Reverse3(0);
      }
    }
  }

  // ------------- INNER <--> RING ------------- //
  else if (particle3_on_lane[0] == INNER_RING[0])
  {

    // ----------------- 0 ----------------- //
    if ((pos3 == INNER_RING_CHECKPOINTS[0] + 1) && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(28, 29) == "1" && button_states.substring(27, 28) == "1")
      {
        copy(TOP_BOTTOM, particle3_on_lane);
        particle.Index3 = TOP_BOTTOM_CHECKPOINTS[0];
        particle.Reverse3(0);
      }
      else if (button_states.substring(28, 29) == "1" && button_states.substring(29, 30) == "1")
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[0];
        particle.Reverse3(0);
      }
      else if (button_states.substring(28, 29) == "1")
      {
        particle.Reverse3(0);
      }
    }
    // ----------------- 1 ----------------- ///
    else if ((pos3 == INNER_RING_CHECKPOINTS[1] - 1) && particle.Direction3 == FORWARD)
    {
      if (button_states.substring(18, 19) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle3_on_lane);
        particle.Index3 = RIGHT_LEFT_CHECKPOINTS[0];
      }
      else if (button_states.substring(18, 19) == "1" && button_states.substring(17, 18) == "1")
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[1];
        particle.Reverse3(0);
      }
      else if (button_states.substring(18, 19) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ----------------- 2 ----------------- ///
    else if ((pos3 == INNER_RING_CHECKPOINTS[2] + 1) && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(20, 21) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle3_on_lane);
        particle.Index3 = RIGHT_LEFT_CHECKPOINTS[0];
        particle.Reverse3(0);
      }
      else if (button_states.substring(20, 21) == "1" && button_states.substring(21, 22) == "1")
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[2];
        particle.Reverse3(0);
      }
      else if (button_states.substring(20, 21) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ----------------- 3 ----------------- //
    else if (( pos3 == INNER_RING_CHECKPOINTS[3] - 1 ) && particle.Direction3 == FORWARD)
    {
      if (button_states.substring(10, 11) == "1" && button_states.substring(11, 12) == "1")
      {
        copy(TOP_BOTTOM, particle3_on_lane);
        particle.Index3 = TOP_BOTTOM_CHECKPOINTS[2];
        particle.Reverse3(0);
      }
      else if (button_states.substring(10, 11) == "1" && button_states.substring(9, 10) == "1" )
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[3];
        particle.Reverse3(0);
      }
      else if (button_states.substring(10, 11) == "1" )
      {
        particle.Reverse3(0);
      }
    }

    // ----------------- 4 ----------------- //
    else if (( pos3 == INNER_RING_CHECKPOINTS[4]  + 1) && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(12, 13) == "1" && button_states.substring(11, 12) == "1" )
      {
        copy(TOP_BOTTOM, particle3_on_lane);
        particle.Index3 = TOP_BOTTOM_CHECKPOINTS[2];
      }
      else if (button_states.substring(12, 13) == "1" && button_states.substring(13, 14) == "1" )
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[4];
        particle.Reverse3(0);
      }
      else if (button_states.substring(12, 13) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ----------------- 5 ----------------- //
    else if (( pos3 == INNER_RING_CHECKPOINTS[5] - 1 ) && particle.Direction3 == FORWARD)
    {
      if (button_states.substring(2, 3) == "1" && button_states.substring(1, 2) == "1" )
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[5];
        particle.Reverse3(0);
      }
      else if (button_states.substring(2, 3) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle3_on_lane);
        particle.Index3 = RIGHT_LEFT_CHECKPOINTS[2];
        particle.Reverse3(0);
      }
      else if (button_states.substring(2, 3) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ----------------- 6 ----------------- //
    else if ((pos3 == INNER_RING_CHECKPOINTS[6]  + 1 ) && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(4, 5) == "1" && button_states.substring(5, 6) == "1" )
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[6];
        particle.Reverse3(0);
      }
      else if (button_states.substring(4, 5) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle3_on_lane);
        particle.Index3 = RIGHT_LEFT_CHECKPOINTS[2];
      }
      else if (button_states.substring(4, 5) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ----------------- 7 ----------------- //
    else if (( pos3 == INNER_RING_CHECKPOINTS[7] - 1 ) && particle.Direction3 == FORWARD)
    {
      if (button_states.substring(26, 27) == "1" && button_states.substring(25, 26) == "1" )
      {
        copy(MID_RING, particle3_on_lane);
        particle.Index3 = MID_RING_CHECKPOINTS[7];
        particle.Reverse3(0);
      }
      else if (button_states.substring(26, 27) == "1" && button_states.substring(27, 28) == "1" )
      {
        copy(TOP_BOTTOM, particle3_on_lane);
        particle.Index3 = TOP_BOTTOM_CHECKPOINTS[0];
      }
      else if (button_states.substring(26, 27) == "1")
      {
        particle.Reverse3(0);
      }
    }
  }

  // ---------------------- MID <--> RING ---------------------- //
  else if ( particle3_on_lane[0] == MID_RING[0] )
  {
    if (( pos3 == MID_RING_CHECKPOINTS[0] + 1 ) && particle.Direction3 == REVERSE)
    {
      // ------------------- 0 ------------------------//
      if (button_states.substring(29, 30) == "1" && button_states.substring(28, 29) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse3(0);
      }
      else if (button_states.substring(29, 30) == "1" && button_states.substring(30, 31) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[0];
        particle.Reverse3(0);
      }
      else if (button_states.substring(29, 30) == "1")
      {
        particle.Reverse3(0);
      }
    }

    else if (( pos3 == MID_RING_CHECKPOINTS[1] - 1 ) && particle.Direction3 == FORWARD)
    {
      // ------------------- 1 ------------------------//
      if (button_states.substring(17, 18) == "1" && button_states.substring(18, 19) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[1];
        particle.Reverse3(0);
      }
      else if (button_states.substring(17, 18) == "1" && button_states.substring(16, 17) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[1];
        particle.Reverse3(0);
      }
      else if (button_states.substring(17, 18) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ------------------- 2 ------------------------//
    else if (( pos3 == MID_RING_CHECKPOINTS[2] + 1 ) && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(21, 22) == "1" && button_states.substring(20, 21) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse3(0);
      }
      else if (button_states.substring(21, 22) == "1" && button_states.substring(22, 23) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[2];
        particle.Reverse3(0);
      }
      else if (button_states.substring(21, 22) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ------------------- 3 ------------------------//
    else if (( pos3 == MID_RING_CHECKPOINTS[3]  - 1 ) && particle.Direction3 == FORWARD)
    {
      if (button_states.substring(9, 10) == "1" && button_states.substring(10, 11) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse3(0);
      }
      else if (button_states.substring(9, 10) == "1" && button_states.substring(8, 9) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[3];
        particle.Reverse3(0);
      }
      else if (button_states.substring(9, 10) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ------------------- 4 ------------------------//
    else if (pos3 == MID_RING_CHECKPOINTS[4] + 1 && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(13, 14) == "1" && button_states.substring(12, 13) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[4];
        particle.Reverse3(0);
      }
      else if (button_states.substring(13, 14) == "1" && button_states.substring(14, 15) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[4];
        particle.Reverse3(0);
      }
      else if (button_states.substring(13, 14) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ------------------- 5 ------------------------//
    else if (( pos3 == MID_RING_CHECKPOINTS[5] - 1 ) && particle.Direction3 == FORWARD)
    {
      if (button_states.substring(1, 2) == "1" && button_states.substring(2, 3) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse3(0);
      }
      else if (button_states.substring(1, 2) == "1" && button_states.substring(0, 1) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[5];
        particle.Reverse3(0);
      }
      else if (button_states.substring(1, 2) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ------------------- 6 ------------------------//
    else if (( pos3 == MID_RING_CHECKPOINTS[6] - 1 ) && particle.Direction3 == REVERSE)
    {
      if (button_states.substring(5, 6) == "1" && button_states.substring(4, 5) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[6];
        particle.Reverse3(0);
      }
      else if (button_states.substring(5, 6) == "1" && button_states.substring(6, 7) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[6];
        particle.Reverse3(0);
      }
      else if (button_states.substring(5, 6) == "1")
      {
        particle.Reverse3(0);
      }
    }

    // ------------------- 7 ------------------------//
    else if (( pos3 == MID_RING_CHECKPOINTS[7] + 1 ) && particle.Direction3 == FORWARD )
    {
      if (button_states.substring(25, 26) == "1" && button_states.substring(26, 27) == "1") {
        copy(INNER_RING, particle3_on_lane);
        particle.Index3 = INNER_RING_CHECKPOINTS[7];
        particle.Reverse3(0);
      }
      else if (button_states.substring(25, 26) == "1" && button_states.substring(24, 25) == "1") {
        copy(OUTER_RING, particle3_on_lane);
        particle.Index3 = OUTER_RING_CHECKPOINTS[7];
        particle.Reverse3(0);
      }
      else if (button_states.substring(25, 26) == "1")
      {
        particle.Reverse3(0);
      }
    }
  }
  else
  {
    //Serial.println("particle1 does not exist inside the lane constrains...");
  }
}

void particle4_check_points()
{

  // ------------- TOP <--> BOTTOM ------------- //
  if (particle4_on_lane[0] == TOP_BOTTOM[0])
  {
    // ----------------- 1 ----------------- //
    if (pos4 == TOP_BOTTOM_CHECKPOINTS[0] && particle.Direction4 == REVERSE )
    {
      if (button_states.substring(27, 28) == "1" && button_states.substring(26, 27) == "1" )
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[7];
        vibrate(2);
      }
      else if (button_states.substring(27, 28) == "1" && button_states.substring(28, 29) == "1" )
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(27, 28) == "1" )
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ----------------- 2 ----------------- //
    else if ((pos4 >= 10) && (pos4 <= 13))
    {
      center_in_place = center.center_in_place();
      center_aligemnt = center.center_alignment();

      // center is off //
      if ( center_in_place )
      {
        if ( particle.Direction4 == REVERSE )
        {
          if (particle1_color == target_colors[0])
          {
            explosion( particle1_color, 1, HIGH );
          }
        }
        else if  (particle.Direction4 == FORWARD )
        {
          if (particle1_color == target_colors[1])
          {
            explosion( particle1_color, 1, HIGH );
          }
        }
      }

      // center is on //
      else if (!center_in_place)
      {
        if ( center_aligemnt == "right" ) {

          if (particle.Direction4 == FORWARD) {
            particle.Index4 = 9;
            particle.Reverse4(0);
          }
          else if ( particle.Direction4 == REVERSE) {
            particle.Index4 = 14;
            particle.Reverse4(0);
          }
        }
        else  {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos4 == 9 or pos4 == 14 ) {
      input_colors_flag1 = LOW;
    }

    // ----------------- 3 ----------------- //
    else if ( pos4 == TOP_BOTTOM_CHECKPOINTS[2] && particle.Direction4 == FORWARD )
    {
      if (button_states.substring(11, 12) == "1" && button_states.substring(10, 11) == "1" )
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(11, 12) == "1" && button_states.substring(12, 13) == "1" )
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[4];
        vibrate(2);
      }
      else if (button_states.substring(11, 12) == "1" )
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }
  }

  // ------------- RIGHT <--> LEFT ------------- //
  if ( particle4_on_lane[0] == RIGHT_LEFT[0] )
  {
    // ------------- 0 ------------- //
    if (pos4 == RIGHT_LEFT_CHECKPOINTS[0] && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(19, 20) == "1" && button_states.substring(20, 21) == "1")
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(19, 20) == "1" && button_states.substring(18, 19) == "1")
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[1];
        vibrate(2);
      }
      else if (button_states.substring(19, 20) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ------------- 1 ------------- //
    else if ((pos4 >= 33) && (pos4 <= 37))
    {
      center_in_place = center.center_in_place();
      center_aligemnt = center.center_alignment();
      Serial.println(center_aligemnt);

      /* center is off */
      if (center_in_place)
      {
        if (particle.Direction4 == FORWARD)
        {
          if (particle1_color == target_colors[3])
          {
            explosion(particle1_color, 1, HIGH);
          }
        }
        else if (particle.Direction4 == REVERSE)
        {
          if (particle1_color == target_colors[2])
          {
            explosion(particle1_color, 1, HIGH);
          }
        }
      }

      /* center is on */
      else if (!center_in_place)
      {
        if ( center_aligemnt == "left" or center_aligemnt == "null" ) { //only temp!!!
          if ( particle.Direction4 == FORWARD ) {
            particle.Index4 = 32;
            particle.Reverse4(0);
          }
          else if ( particle.Direction4 == REVERSE) {
            particle.Index4 = 38;
            particle.Reverse4(0);
          }
        }
        else {
          Serial.println ("letting particle pass...");
        }
      }
    }

    else if ( pos4 == 32 or pos4 == 38 ) {
      input_colors_flag1 = LOW;
    }

    // ------------- 2 ------------- //
    else if (pos4 == RIGHT_LEFT_CHECKPOINTS[2] && particle.Direction4 == FORWARD)
    {
      if (button_states.substring(3, 4) == "1" && button_states.substring(2, 3) == "1" )
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(3, 4) == "1" && button_states.substring(4, 5) == "1" )
      {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[6];
        vibrate(2);
      }
      else if (button_states.substring(3, 4) == "1" )
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }
  }

  // ------------- INNER <--> RING ------------- //
  else if (particle4_on_lane[0] == INNER_RING[0])
  {

    // ----------------- 0 ----------------- //
    if ((pos4 == INNER_RING_CHECKPOINTS[0] + 1) && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(28, 29) == "1" && button_states.substring(27, 28) == "1")
      {
        copy(TOP_BOTTOM, particle4_on_lane);
        particle.Index4 = TOP_BOTTOM_CHECKPOINTS[0];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(28, 29) == "1" && button_states.substring(29, 30) == "1")
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[0];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(28, 29) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }
    // ----------------- 1 ----------------- ///
    else if ((pos4 == INNER_RING_CHECKPOINTS[1] - 1) && particle.Direction4 == FORWARD)
    {
      if (button_states.substring(18, 19) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle4_on_lane);
        particle.Index4 = RIGHT_LEFT_CHECKPOINTS[0];
        vibrate(2);
      }
      else if (button_states.substring(18, 19) == "1" && button_states.substring(17, 18) == "1")
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[1];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(18, 19) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ----------------- 2 ----------------- ///
    else if ((pos4 == INNER_RING_CHECKPOINTS[2] + 1) && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(20, 21) == "1" && button_states.substring(19, 20) == "1")
      {
        copy(RIGHT_LEFT, particle4_on_lane);
        particle.Index4 = RIGHT_LEFT_CHECKPOINTS[0];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(20, 21) == "1" && button_states.substring(21, 22) == "1")
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[2];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(20, 21) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ----------------- 3 ----------------- //
    else if (( pos4 == INNER_RING_CHECKPOINTS[3] - 1 ) && particle.Direction4 == FORWARD)
    {
      if (button_states.substring(10, 11) == "1" && button_states.substring(11, 12) == "1")
      {
        copy(TOP_BOTTOM, particle4_on_lane);
        particle.Index4 = TOP_BOTTOM_CHECKPOINTS[2];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(10, 11) == "1" && button_states.substring(9, 10) == "1" )
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[3];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(10, 11) == "1" )
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ----------------- 4 ----------------- //
    else if (( pos4 == INNER_RING_CHECKPOINTS[4]  + 1) && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(12, 13) == "1" && button_states.substring(11, 12) == "1" )
      {
        copy(TOP_BOTTOM, particle4_on_lane);
        particle.Index4 = TOP_BOTTOM_CHECKPOINTS[2];
        vibrate(2);
      }
      else if (button_states.substring(12, 13) == "1" && button_states.substring(13, 14) == "1" )
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[4];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(12, 13) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ----------------- 5 ----------------- //
    else if (( pos4 == INNER_RING_CHECKPOINTS[5] - 1 ) && particle.Direction4 == FORWARD)
    {
      if (button_states.substring(2, 3) == "1" && button_states.substring(1, 2) == "1" )
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[5];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(2, 3) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle4_on_lane);
        particle.Index4 = RIGHT_LEFT_CHECKPOINTS[2];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(2, 3) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ----------------- 6 ----------------- //
    else if ((pos4 == INNER_RING_CHECKPOINTS[6]  + 1 ) && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(4, 5) == "1" && button_states.substring(5, 6) == "1" )
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[6];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(4, 5) == "1" && button_states.substring(3, 4) == "1" )
      {
        copy(RIGHT_LEFT, particle4_on_lane);
        particle.Index4 = RIGHT_LEFT_CHECKPOINTS[2];
        vibrate(2);
      }
      else if (button_states.substring(4, 5) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ----------------- 7 ----------------- //
    else if (( pos4 == INNER_RING_CHECKPOINTS[7] - 1 ) && particle.Direction4 == FORWARD)
    {
      if (button_states.substring(26, 27) == "1" && button_states.substring(25, 26) == "1" )
      {
        copy(MID_RING, particle4_on_lane);
        particle.Index4 = MID_RING_CHECKPOINTS[7];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(26, 27) == "1" && button_states.substring(27, 28) == "1" )
      {
        copy(TOP_BOTTOM, particle4_on_lane);
        particle.Index4 = TOP_BOTTOM_CHECKPOINTS[0];
        vibrate(2);
      }
      else if (button_states.substring(26, 27) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }
  }

  // ---------------------- MID <--> RING ---------------------- //
  else if ( particle4_on_lane[0] == MID_RING[0] )
  {
    if (( pos4 == MID_RING_CHECKPOINTS[0] + 1 ) && particle.Direction4 == REVERSE)
    {
      // ------------------- 0 ------------------------//
      if (button_states.substring(29, 30) == "1" && button_states.substring(28, 29) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[0];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(29, 30) == "1" && button_states.substring(30, 31) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[0];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(29, 30) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    else if (( pos4 == MID_RING_CHECKPOINTS[1] - 1 ) && particle.Direction4 == FORWARD)
    {
      // ------------------- 1 ------------------------//
      if (button_states.substring(17, 18) == "1" && button_states.substring(18, 19) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[1];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(17, 18) == "1" && button_states.substring(16, 17) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[1];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(17, 18) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ------------------- 2 ------------------------//
    else if (( pos4 == MID_RING_CHECKPOINTS[2] + 1 ) && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(21, 22) == "1" && button_states.substring(20, 21) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[2];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(21, 22) == "1" && button_states.substring(22, 23) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[2];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(21, 22) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ------------------- 3 ------------------------//
    else if (( pos4 == MID_RING_CHECKPOINTS[3]  - 1 ) && particle.Direction4 == FORWARD)
    {
      if (button_states.substring(9, 10) == "1" && button_states.substring(10, 11) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[3];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(9, 10) == "1" && button_states.substring(8, 9) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[3];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(9, 10) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ------------------- 4 ------------------------//
    else if (pos4 == MID_RING_CHECKPOINTS[4] + 1 && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(13, 14) == "1" && button_states.substring(12, 13) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[4];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(13, 14) == "1" && button_states.substring(14, 15) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[4];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(13, 14) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ------------------- 5 ------------------------//
    else if (( pos4 == MID_RING_CHECKPOINTS[5] - 1 ) && particle.Direction4 == FORWARD)
    {
      if (button_states.substring(1, 2) == "1" && button_states.substring(2, 3) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[5];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(1, 2) == "1" && button_states.substring(0, 1) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[5];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(1, 2) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ------------------- 6 ------------------------//
    else if (( pos4 == MID_RING_CHECKPOINTS[6] - 1 ) && particle.Direction4 == REVERSE)
    {
      if (button_states.substring(5, 6) == "1" && button_states.substring(4, 5) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[6];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(5, 6) == "1" && button_states.substring(6, 7) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[6];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(5, 6) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }

    // ------------------- 7 ------------------------//
    else if (( pos4 == MID_RING_CHECKPOINTS[7] + 1 ) && particle.Direction4 == FORWARD )
    {
      if (button_states.substring(25, 26) == "1" && button_states.substring(26, 27) == "1") {
        copy(INNER_RING, particle4_on_lane);
        particle.Index4 = INNER_RING_CHECKPOINTS[7];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(25, 26) == "1" && button_states.substring(24, 25) == "1") {
        copy(OUTER_RING, particle4_on_lane);
        particle.Index4 = OUTER_RING_CHECKPOINTS[7];
        particle.Reverse4(0);
        vibrate(2);
      }
      else if (button_states.substring(25, 26) == "1")
      {
        particle.Reverse4(0);
        vibrate(1);
      }
    }
  }
  else
  {
    //Serial.println("particle1 does not exist inside the lane constrains...");
  }
}

void particle5_check_points() {}


CRGB indicator_color = CRGB( 20, 25, 30 );

void button_indicator_update() {
  if (button_states.substring(0, 1) == "1") {
    leds[OUTER_RING_CHECKPOINTS[5]] = indicator_color;
  }
  if (button_states.substring(1, 2) == "1") {
    leds[MID_RING_CHECKPOINTS[5]] = indicator_color;
  }
  if (button_states.substring(2, 3) == "1") {
    leds[INNER_RING_CHECKPOINTS[5]] = indicator_color;
  }
  if (button_states.substring(3, 4) == "1") {
    leds[RIGHT_LEFT_CHECKPOINTS[2]] = indicator_color;
  }
  if (button_states.substring(4, 5) == "1") {
    leds[INNER_RING_CHECKPOINTS[6]] = indicator_color;
  }
  if (button_states.substring(5, 6) == "1") {
    leds[MID_RING_CHECKPOINTS[6]] = indicator_color;
  }
  if (button_states.substring(6, 7) == "1") {
    leds[OUTER_RING_CHECKPOINTS[6]] = indicator_color;
  }
  if (button_states.substring(8, 9) == "1") {
    leds[OUTER_RING_CHECKPOINTS[3]] = indicator_color;
  }
  if (button_states.substring(9, 10) == "1") {
    leds[MID_RING_CHECKPOINTS[3]] = indicator_color;
  }
  if (button_states.substring(10, 11) == "1") {
    leds[INNER_RING_CHECKPOINTS[3]] = indicator_color;
  }
  if (button_states.substring(11, 12) == "1") {
    leds[TOP_BOTTOM_CHECKPOINTS[2]] = indicator_color;
  }
  if (button_states.substring(12, 13) == "1") {
    leds[INNER_RING_CHECKPOINTS[4]] = indicator_color;
  }
  if (button_states.substring(13, 14) == "1") {
    leds[MID_RING_CHECKPOINTS[4]] = indicator_color;
  }
  if (button_states.substring(14, 15) == "1") {
    leds[OUTER_RING_CHECKPOINTS[4]] = indicator_color;
  }
  if (button_states.substring(16, 17) == "1") {
    leds[OUTER_RING_CHECKPOINTS[1]] = indicator_color;
  }
  if (button_states.substring(17, 18) == "1") {
    leds[MID_RING_CHECKPOINTS[1]] = indicator_color;
  }
  if (button_states.substring(18, 19) == "1") {
    leds[INNER_RING_CHECKPOINTS[1]] = indicator_color;
  }
  if (button_states.substring(19, 20) == "1") {
    leds[RIGHT_LEFT_CHECKPOINTS[0]] = indicator_color;
  }
  if (button_states.substring(20, 21) == "1") {
    leds[INNER_RING_CHECKPOINTS[2]] = indicator_color;
  }
  if (button_states.substring(21, 22) == "1") {
    leds[MID_RING_CHECKPOINTS[2]] = indicator_color;
  }
  if (button_states.substring(22, 23) == "1") {
    leds[OUTER_RING_CHECKPOINTS[2]] = indicator_color;
  }
  if (button_states.substring(24, 25) == "1") {
    leds[OUTER_RING_CHECKPOINTS[7]] = indicator_color;
  }
  if (button_states.substring(25, 26) == "1") {
    leds[MID_RING_CHECKPOINTS[7]] = indicator_color;
  }
  if (button_states.substring(26, 27) == "1") {
    leds[INNER_RING_CHECKPOINTS[7]] = indicator_color;
  }
  if (button_states.substring(27, 28) == "1") {
    leds[TOP_BOTTOM_CHECKPOINTS[0]] = indicator_color;
  }
  if (button_states.substring(28, 29) == "1") {
    leds[INNER_RING_CHECKPOINTS[0]] = indicator_color;
  }
  if (button_states.substring(29, 30) == "1") {
    leds[MID_RING_CHECKPOINTS[0]] = indicator_color;
  }
  if (button_states.substring(30, 31) == "1") {
    leds[OUTER_RING_CHECKPOINTS[0]] = indicator_color;
  }
}

int fade_out_by;
int color_reset_interval = 15000;
int fade_interval = color_reset_interval / 5;

byte limitXa = 16;
byte limitXb = 7;
byte limitYa = 40;
byte limitYb = 31;

unsigned long last_update_fade;       // milliseconds between updates
unsigned long lastUpdate;             // last update of position

void targets() {

  if ((millis() - lastUpdate) > color_reset_interval) // time to update
  {
    lastUpdate = millis();
    Serial.print("Time to set new colors...");

    limitXa = 16;
    limitXb = 7;
    limitYa = 40;
    limitYb = 31;

    reset_center_colors();

  }
  else if ((millis() - last_update_fade) > fade_interval)
  {
    last_update_fade = millis();
    Serial.println("Lets remove one pixel...");
    
    limitXa = limitXa - 1;
    limitXb = limitXb + 1;
    limitYa = limitYa - 1;
    limitYb = limitYb + 1;
  }

  switch (active_particles) {
    case 1:
      if (random_target_position == 1) {
        for (int x = 12; x <= limitXa; x++) {
          leds[x] = target_colors[0];
        }
      }
      else if (random_target_position == 2) {
        for (int x = 11; x >= limitXb; x--) {
          leds[x] = target_colors[0];
        }
      }
      else if (random_target_position == 3) {
        for (int x = 36; x <= limitYa; x++) {
          leds[x] = target_colors[0];
        }
      }
      else if (random_target_position == 4) {
        for (int x = 35; x >= limitYb; x--) {
          leds[x] = target_colors[0];
        }
      }
      break;

    case 2:
      if ( random_target_position == 1 ) {
        leds[12] = target_colors[0];
        leds[11] = target_colors[1];
      }
      else if ( random_target_position == 2 ) {
        leds[11] = target_colors[0];
        leds[12] = target_colors[1];
      }
      else if ( random_target_position == 3 ) {
        leds[36] = target_colors[1];
        leds[35] = target_colors[0];
      }
      else if ( random_target_position == 4 ) {
        leds[35] = target_colors[1];
        leds[36] = target_colors[0];
      }
      break;

    case 3:
      if (random_target_position == 1) {
        leds[12] = target_colors[0];
        leds[11] = target_colors[1];
        leds[36] = target_colors[2];
      }
      else if (random_target_position == 2) {
        leds[12] = target_colors[0];
        leds[11] = target_colors[1];
        leds[35] = target_colors[2];
      }
      else if (random_target_position == 3) {
        leds[36] = target_colors[0];
        leds[35] = target_colors[1];
        leds[11] = target_colors[2];
      }
      else if (random_target_position == 4) {
        leds[36] = target_colors[0];
        leds[35] = target_colors[1];
        leds[12] = target_colors[2];
      }
      break;

    case 4:
      if (random_target_position == 1) {
        leds[12] = target_colors[0];
        leds[11] = target_colors[1];
        leds[36] = target_colors[2];
        leds[35] = target_colors[3];
      }
      else if (random_target_position == 2) {
        leds[11] = target_colors[0];
        leds[36] = target_colors[1];
        leds[35] = target_colors[2];
        leds[12] = target_colors[3];
      }
      else if (random_target_position == 3) {
        leds[36] = target_colors[0];
        leds[35] = target_colors[1];
        leds[12] = target_colors[2];
        leds[11] = target_colors[3];
      }
      else if (random_target_position == 4) {
        leds[35] = target_colors[0];
        leds[12] = target_colors[1];
        leds[11] = target_colors[2];
        leds[36] = target_colors[3];
      }
      break;

    default:
      Serial.println("no center values inputed...");
      break;
  }
}

bool game_1_init_flag = 1;

void game_1_init_sequence() {

  if (game_1_init_flag) {

    particle.particle_1_init(54, FORWARD); // set init: speed, direction
    particle.particle_2_init(54, REVERSE);
    particle.particle_3_init(54, FORWARD);
    particle.particle_4_init(54, REVERSE);

    //------------- copys array (param 0) to array (param 1) ------------//
    copy(INNER_RING, particle1_on_lane);
    copy(INNER_RING, particle2_on_lane);
    copy(MID_RING, particle3_on_lane);
    copy(MID_RING, particle4_on_lane);
    copy(MID_RING, particle5_on_lane);

    reset_center_colors();

    game_1_init_flag = 0;
  }
}

#endif // !_COLLIDOR_GAME_CONTROLS_H
